/*
 InstaLate (instant translation app)
 Copyright (C) 2021 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.unn;

import android.text.SpannableStringBuilder;

// This convenience class mostly exists to workaround an unfixed Robolectric bug where
// SpannableStringBuilder.append(text, what, flags) would throw a NoSuchMethodError.
// See https://github.com/robolectric/robolectric/issues/3225
public class MySpannableStringBuilder extends SpannableStringBuilder {

    public MySpannableStringBuilder() {
    }

    public MySpannableStringBuilder(CharSequence text, Object what, int flags) {
        append(text);
        setSpan(what, 0, length(), flags);
    }

    @Override
    public SpannableStringBuilder append(CharSequence text, Object what, int flags) {
        int start = length();
        append(text);
        setSpan(what, start, length(), flags);
        return this;
    }
}
