/*
 InstaLate (instant translation app)
 Copyright (C) 2021 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.unn;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.Map;

public class PageResponse implements Parcelable {

    private String mResponse = "";
    private Map<String, String> mErrors = new HashMap<>();   // Note: AsyncTask "swallows exceptions from doInBackground()" so we handle errors ourselves


    public PageResponse() {

    }

    public String get() {
        return mResponse;
    }
    public void set(String response) {
        mResponse = response;
    }

    public void setError(String error) {
        setError(error, "");
    }
    public void setError(String error, String extra) {
        mErrors.put(error, extra);
    }
    public boolean hasError(String error) {
        return mErrors.containsKey(error);
    }
    public boolean hasErrors() {
        return mErrors.size() != 0;
    }
    public Map<String, String> getErrors() {
        return mErrors;
    }
    public String getErrorExtra(String error) {
        return mErrors.get(error);
    }
    public void clearErrors() {
        mErrors.clear();
    }


    protected PageResponse(Parcel in) {
        mResponse = in.readString();
        mErrors = DroidUtils.readStringMapFromParcel(in, mErrors);
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mResponse);
        DroidUtils.writeStringMapToParcel(dest, mErrors);
    }
    public static final Creator<PageResponse> CREATOR = new Creator<PageResponse>() {
        @Override
        public PageResponse createFromParcel(Parcel in) {
            return new PageResponse(in);
        }

        @Override
        public PageResponse[] newArray(int size) {
            return new PageResponse[size];
        }
    };
    @Override
    public int describeContents() {
        return 0;
    }
}
