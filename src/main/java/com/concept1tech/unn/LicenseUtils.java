/*
 InstaLate (instant translation app)
 Copyright (C) 2021 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.unn;

import java.util.Locale;

public class LicenseUtils {

    public static String mit(String year, String owner) {
        return "The MIT License (MIT)\n" +
                    String.format(Locale.US, "Copyright © %s %s\n", year, owner) +
                    "\n" +
                    "Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:\n" +
                    "\n" +
                    "The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.\n" +
                    "\n" +
                    "THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.";
    }

    public static String apache2() {
        return "Apache License\n" +
                    "Version 2.0, January 2004\n" +
                    "http://www.apache.org/licenses/\n" +
                    "\n" +
                    "TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION\n" +
                    "\n" +
                    "1. Definitions.\n" +
                    "\n" +
                    "\"License\" shall mean the terms and conditions for use, reproduction, and distribution as defined by Sections 1 through 9 of this document.\n" +
                    "\n" +
                    "\"Licensor\" shall mean the copyright owner or entity authorized by the copyright owner that is granting the License.\n" +
                    "\n" +
                    "\"Legal Entity\" shall mean the union of the acting entity and all other entities that control, are controlled by, or are under common control with that entity. For the purposes of this definition, \"control\" means (i) the power, direct or indirect, to cause the direction or management of such entity, whether by contract or otherwise, or (ii) ownership of fifty percent (50%) or more of the outstanding shares, or (iii) beneficial ownership of such entity.\n" +
                    "\n" +
                    "\"You\" (or \"Your\") shall mean an individual or Legal Entity exercising permissions granted by this License.\n" +
                    "\n" +
                    "\"Source\" form shall mean the preferred form for making modifications, including but not limited to software source code, documentation source, and configuration files.\n" +
                    "\n" +
                    "\"Object\" form shall mean any form resulting from mechanical transformation or translation of a Source form, including but not limited to compiled object code, generated documentation, and conversions to other media types.\n" +
                    "\n" +
                    "\"Work\" shall mean the work of authorship, whether in Source or Object form, made available under the License, as indicated by a copyright notice that is included in or attached to the work (an example is provided in the Appendix below).\n" +
                    "\n" +
                    "\"Derivative Works\" shall mean any work, whether in Source or Object form, that is based on (or derived from) the Work and for which the editorial revisions, annotations, elaborations, or other modifications represent, as a whole, an original work of authorship. For the purposes of this License, Derivative Works shall not include works that remain separable from, or merely link (or bind by name) to the interfaces of, the Work and Derivative Works thereof.\n" +
                    "\n" +
                    "\"Contribution\" shall mean any work of authorship, including the original version of the Work and any modifications or additions to that Work or Derivative Works thereof, that is intentionally submitted to Licensor for inclusion in the Work by the copyright owner or by an individual or Legal Entity authorized to submit on behalf of the copyright owner. For the purposes of this definition, \"submitted\" means any form of electronic, verbal, or written communication sent to the Licensor or its representatives, including but not limited to communication on electronic mailing lists, source code control systems, and issue tracking systems that are managed by, or on behalf of, the Licensor for the purpose of discussing and improving the Work, but excluding communication that is conspicuously marked or otherwise designated in writing by the copyright owner as \"Not a Contribution.\"\n" +
                    "\n" +
                    "\"Contributor\" shall mean Licensor and any individual or Legal Entity on behalf of whom a Contribution has been received by Licensor and subsequently incorporated within the Work.\n" +
                    "\n" +
                    "2. Grant of Copyright License.\n" +
                    "\n" +
                    "Subject to the terms and conditions of this License, each Contributor hereby grants to You a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable copyright license to reproduce, prepare Derivative Works of, publicly display, publicly perform, sublicense, and distribute the Work and such Derivative Works in Source or Object form.\n" +
                    "\n" +
                    "3. Grant of Patent License.\n" +
                    "\n" +
                    "Subject to the terms and conditions of this License, each Contributor hereby grants to You a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable (except as stated in this section) patent license to make, have made, use, offer to sell, sell, import, and otherwise transfer the Work, where such license applies only to those patent claims licensable by such Contributor that are necessarily infringed by their Contribution(s) alone or by combination of their Contribution(s) with the Work to which such Contribution(s) was submitted. If You institute patent litigation against any entity (including a cross-claim or counterclaim in a lawsuit) alleging that the Work or a Contribution incorporated within the Work constitutes direct or contributory patent infringement, then any patent licenses granted to You under this License for that Work shall terminate as of the date such litigation is filed.\n" +
                    "\n" +
                    "4. Redistribution.\n" +
                    "\n" +
                    "You may reproduce and distribute copies of the Work or Derivative Works thereof in any medium, with or without modifications, and in Source or Object form, provided that You meet the following conditions:\n" +
                    "\n" +
                    "You must give any other recipients of the Work or Derivative Works a copy of this License; and\n" +
                    "You must cause any modified files to carry prominent notices stating that You changed the files; and\n" +
                    "You must retain, in the Source form of any Derivative Works that You distribute, all copyright, patent, trademark, and attribution notices from the Source form of the Work, excluding those notices that do not pertain to any part of the Derivative Works; and\n" +
                    "If the Work includes a \"NOTICE\" text file as part of its distribution, then any Derivative Works that You distribute must include a readable copy of the attribution notices contained within such NOTICE file, excluding those notices that do not pertain to any part of the Derivative Works, in at least one of the following places: within a NOTICE text file distributed as part of the Derivative Works; within the Source form or documentation, if provided along with the Derivative Works; or, within a display generated by the Derivative Works, if and wherever such third-party notices normally appear. The contents of the NOTICE file are for informational purposes only and do not modify the License. You may add Your own attribution notices within Derivative Works that You distribute, alongside or as an addendum to the NOTICE text from the Work, provided that such additional attribution notices cannot be construed as modifying the License.\n" +
                    "You may add Your own copyright statement to Your modifications and may provide additional or different license terms and conditions for use, reproduction, or distribution of Your modifications, or for any such Derivative Works as a whole, provided Your use, reproduction, and distribution of the Work otherwise complies with the conditions stated in this License.\n" +
                    "\n" +
                    "5. Submission of Contributions.\n" +
                    "\n" +
                    "Unless You explicitly state otherwise, any Contribution intentionally submitted for inclusion in the Work by You to the Licensor shall be under the terms and conditions of this License, without any additional terms or conditions. Notwithstanding the above, nothing herein shall supersede or modify the terms of any separate license agreement you may have executed with Licensor regarding such Contributions.\n" +
                    "\n" +
                    "6. Trademarks.\n" +
                    "\n" +
                    "This License does not grant permission to use the trade names, trademarks, service marks, or product names of the Licensor, except as required for reasonable and customary use in describing the origin of the Work and reproducing the content of the NOTICE file.\n" +
                    "\n" +
                    "7. Disclaimer of Warranty.\n" +
                    "\n" +
                    "Unless required by applicable law or agreed to in writing, Licensor provides the Work (and each Contributor provides its Contributions) on an \"AS IS\" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied, including, without limitation, any warranties or conditions of TITLE, NON-INFRINGEMENT, MERCHANTABILITY, or FITNESS FOR A PARTICULAR PURPOSE. You are solely responsible for determining the appropriateness of using or redistributing the Work and assume any risks associated with Your exercise of permissions under this License.\n" +
                    "\n" +
                    "8. Limitation of Liability.\n" +
                    "\n" +
                    "In no event and under no legal theory, whether in tort (including negligence), contract, or otherwise, unless required by applicable law (such as deliberate and grossly negligent acts) or agreed to in writing, shall any Contributor be liable to You for damages, including any direct, indirect, special, incidental, or consequential damages of any character arising as a result of this License or out of the use or inability to use the Work (including but not limited to damages for loss of goodwill, work stoppage, computer failure or malfunction, or any and all other commercial damages or losses), even if such Contributor has been advised of the possibility of such damages.\n" +
                    "\n" +
                    "9. Accepting Warranty or Additional Liability.\n" +
                    "\n" +
                    "While redistributing the Work or Derivative Works thereof, You may choose to offer, and charge a fee for, acceptance of support, warranty, indemnity, or other liability obligations and/or rights consistent with this License. However, in accepting such obligations, You may act only on Your own behalf and on Your sole responsibility, not on behalf of any other Contributor, and only if You agree to indemnify, defend, and hold each Contributor harmless for any liability incurred by, or claims asserted against, such Contributor by reason of your accepting any such warranty or additional liability.\n" +
                    "\n" +
                    "END OF TERMS AND CONDITIONS\n" +
                    "\n" +
                    "APPENDIX: How to apply the Apache License to your work\n" +
                    "\n" +
                    "To apply the Apache License to your work, attach the following boilerplate notice, with the fields enclosed by brackets \"[]\" replaced with your own identifying information. (Don't include the brackets!) The text should be enclosed in the appropriate comment syntax for the file format. We also recommend that a file or class name and description of purpose be included on the same \"printed page\" as the copyright notice for easier identification within third-party archives.\n" +
                    "\n" +
                    "   Copyright [yyyy] [name of copyright owner]\n" +
                    "\n" +
                    "   Licensed under the Apache License, Version 2.0 (the \"License\");\n" +
                    "   you may not use this file except in compliance with the License.\n" +
                    "   You may obtain a copy of the License at\n" +
                    "\n" +
                    "     http://www.apache.org/licenses/LICENSE-2.0\n" +
                    "\n" +
                    "   Unless required by applicable law or agreed to in writing, software\n" +
                    "   distributed under the License is distributed on an \"AS IS\" BASIS,\n" +
                    "   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n" +
                    "   See the License for the specific language governing permissions and\n" +
                    "   limitations under the License.";
    }

    public static String gplv2() {
        return "### GNU GENERAL PUBLIC LICENSE\n" +
                    "\n" +
                    "Version 2, June 1991\n" +
                    "\n" +
                    "Copyright (C) 1989, 1991 Free Software Foundation, Inc.  \n" +
                    "51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA\n" +
                    "\n" +
                    "Everyone is permitted to copy and distribute verbatim copies\n" +
                    "of this license document, but changing it is not allowed.\n" +
                    "\n" +
                    "### Preamble\n" +
                    "\n" +
                    "The licenses for most software are designed to take away your freedom\n" +
                    "to share and change it. By contrast, the GNU General Public License is\n" +
                    "intended to guarantee your freedom to share and change free\n" +
                    "software--to make sure the software is free for all its users. This\n" +
                    "General Public License applies to most of the Free Software\n" +
                    "Foundation's software and to any other program whose authors commit to\n" +
                    "using it. (Some other Free Software Foundation software is covered by\n" +
                    "the GNU Lesser General Public License instead.) You can apply it to\n" +
                    "your programs, too.\n" +
                    "\n" +
                    "When we speak of free software, we are referring to freedom, not\n" +
                    "price. Our General Public Licenses are designed to make sure that you\n" +
                    "have the freedom to distribute copies of free software (and charge for\n" +
                    "this service if you wish), that you receive source code or can get it\n" +
                    "if you want it, that you can change the software or use pieces of it\n" +
                    "in new free programs; and that you know you can do these things.\n" +
                    "\n" +
                    "To protect your rights, we need to make restrictions that forbid\n" +
                    "anyone to deny you these rights or to ask you to surrender the rights.\n" +
                    "These restrictions translate to certain responsibilities for you if\n" +
                    "you distribute copies of the software, or if you modify it.\n" +
                    "\n" +
                    "For example, if you distribute copies of such a program, whether\n" +
                    "gratis or for a fee, you must give the recipients all the rights that\n" +
                    "you have. You must make sure that they, too, receive or can get the\n" +
                    "source code. And you must show them these terms so they know their\n" +
                    "rights.\n" +
                    "\n" +
                    "We protect your rights with two steps: (1) copyright the software, and\n" +
                    "(2) offer you this license which gives you legal permission to copy,\n" +
                    "distribute and/or modify the software.\n" +
                    "\n" +
                    "Also, for each author's protection and ours, we want to make certain\n" +
                    "that everyone understands that there is no warranty for this free\n" +
                    "software. If the software is modified by someone else and passed on,\n" +
                    "we want its recipients to know that what they have is not the\n" +
                    "original, so that any problems introduced by others will not reflect\n" +
                    "on the original authors' reputations.\n" +
                    "\n" +
                    "Finally, any free program is threatened constantly by software\n" +
                    "patents. We wish to avoid the danger that redistributors of a free\n" +
                    "program will individually obtain patent licenses, in effect making the\n" +
                    "program proprietary. To prevent this, we have made it clear that any\n" +
                    "patent must be licensed for everyone's free use or not licensed at\n" +
                    "all.\n" +
                    "\n" +
                    "The precise terms and conditions for copying, distribution and\n" +
                    "modification follow.\n" +
                    "\n" +
                    "### TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION\n" +
                    "\n" +
                    "**0.** This License applies to any program or other work which\n" +
                    "contains a notice placed by the copyright holder saying it may be\n" +
                    "distributed under the terms of this General Public License. The\n" +
                    "\"Program\", below, refers to any such program or work, and a \"work\n" +
                    "based on the Program\" means either the Program or any derivative work\n" +
                    "under copyright law: that is to say, a work containing the Program or\n" +
                    "a portion of it, either verbatim or with modifications and/or\n" +
                    "translated into another language. (Hereinafter, translation is\n" +
                    "included without limitation in the term \"modification\".) Each licensee\n" +
                    "is addressed as \"you\".\n" +
                    "\n" +
                    "Activities other than copying, distribution and modification are not\n" +
                    "covered by this License; they are outside its scope. The act of\n" +
                    "running the Program is not restricted, and the output from the Program\n" +
                    "is covered only if its contents constitute a work based on the Program\n" +
                    "(independent of having been made by running the Program). Whether that\n" +
                    "is true depends on what the Program does.\n" +
                    "\n" +
                    "**1.** You may copy and distribute verbatim copies of the Program's\n" +
                    "source code as you receive it, in any medium, provided that you\n" +
                    "conspicuously and appropriately publish on each copy an appropriate\n" +
                    "copyright notice and disclaimer of warranty; keep intact all the\n" +
                    "notices that refer to this License and to the absence of any warranty;\n" +
                    "and give any other recipients of the Program a copy of this License\n" +
                    "along with the Program.\n" +
                    "\n" +
                    "You may charge a fee for the physical act of transferring a copy, and\n" +
                    "you may at your option offer warranty protection in exchange for a\n" +
                    "fee.\n" +
                    "\n" +
                    "**2.** You may modify your copy or copies of the Program or any\n" +
                    "portion of it, thus forming a work based on the Program, and copy and\n" +
                    "distribute such modifications or work under the terms of Section 1\n" +
                    "above, provided that you also meet all of these conditions:\n" +
                    "\n" +
                    "  \n" +
                    "**a)** You must cause the modified files to carry prominent notices\n" +
                    "stating that you changed the files and the date of any change.\n" +
                    "\n" +
                    "  \n" +
                    "**b)** You must cause any work that you distribute or publish, that in\n" +
                    "whole or in part contains or is derived from the Program or any part\n" +
                    "thereof, to be licensed as a whole at no charge to all third parties\n" +
                    "under the terms of this License.\n" +
                    "\n" +
                    "  \n" +
                    "**c)** If the modified program normally reads commands interactively\n" +
                    "when run, you must cause it, when started running for such interactive\n" +
                    "use in the most ordinary way, to print or display an announcement\n" +
                    "including an appropriate copyright notice and a notice that there is\n" +
                    "no warranty (or else, saying that you provide a warranty) and that\n" +
                    "users may redistribute the program under these conditions, and telling\n" +
                    "the user how to view a copy of this License. (Exception: if the\n" +
                    "Program itself is interactive but does not normally print such an\n" +
                    "announcement, your work based on the Program is not required to print\n" +
                    "an announcement.)\n" +
                    "\n" +
                    "These requirements apply to the modified work as a whole. If\n" +
                    "identifiable sections of that work are not derived from the Program,\n" +
                    "and can be reasonably considered independent and separate works in\n" +
                    "themselves, then this License, and its terms, do not apply to those\n" +
                    "sections when you distribute them as separate works. But when you\n" +
                    "distribute the same sections as part of a whole which is a work based\n" +
                    "on the Program, the distribution of the whole must be on the terms of\n" +
                    "this License, whose permissions for other licensees extend to the\n" +
                    "entire whole, and thus to each and every part regardless of who wrote\n" +
                    "it.\n" +
                    "\n" +
                    "Thus, it is not the intent of this section to claim rights or contest\n" +
                    "your rights to work written entirely by you; rather, the intent is to\n" +
                    "exercise the right to control the distribution of derivative or\n" +
                    "collective works based on the Program.\n" +
                    "\n" +
                    "In addition, mere aggregation of another work not based on the Program\n" +
                    "with the Program (or with a work based on the Program) on a volume of\n" +
                    "a storage or distribution medium does not bring the other work under\n" +
                    "the scope of this License.\n" +
                    "\n" +
                    "**3.** You may copy and distribute the Program (or a work based on it,\n" +
                    "under Section 2) in object code or executable form under the terms of\n" +
                    "Sections 1 and 2 above provided that you also do one of the following:\n" +
                    "\n" +
                    "  \n" +
                    "**a)** Accompany it with the complete corresponding machine-readable\n" +
                    "source code, which must be distributed under the terms of Sections 1\n" +
                    "and 2 above on a medium customarily used for software interchange; or,\n" +
                    "\n" +
                    "  \n" +
                    "**b)** Accompany it with a written offer, valid for at least three\n" +
                    "years, to give any third party, for a charge no more than your cost of\n" +
                    "physically performing source distribution, a complete machine-readable\n" +
                    "copy of the corresponding source code, to be distributed under the\n" +
                    "terms of Sections 1 and 2 above on a medium customarily used for\n" +
                    "software interchange; or,\n" +
                    "\n" +
                    "  \n" +
                    "**c)** Accompany it with the information you received as to the offer\n" +
                    "to distribute corresponding source code. (This alternative is allowed\n" +
                    "only for noncommercial distribution and only if you received the\n" +
                    "program in object code or executable form with such an offer, in\n" +
                    "accord with Subsection b above.)\n" +
                    "\n" +
                    "The source code for a work means the preferred form of the work for\n" +
                    "making modifications to it. For an executable work, complete source\n" +
                    "code means all the source code for all modules it contains, plus any\n" +
                    "associated interface definition files, plus the scripts used to\n" +
                    "control compilation and installation of the executable. However, as a\n" +
                    "special exception, the source code distributed need not include\n" +
                    "anything that is normally distributed (in either source or binary\n" +
                    "form) with the major components (compiler, kernel, and so on) of the\n" +
                    "operating system on which the executable runs, unless that component\n" +
                    "itself accompanies the executable.\n" +
                    "\n" +
                    "If distribution of executable or object code is made by offering\n" +
                    "access to copy from a designated place, then offering equivalent\n" +
                    "access to copy the source code from the same place counts as\n" +
                    "distribution of the source code, even though third parties are not\n" +
                    "compelled to copy the source along with the object code.\n" +
                    "\n" +
                    "**4.** You may not copy, modify, sublicense, or distribute the Program\n" +
                    "except as expressly provided under this License. Any attempt otherwise\n" +
                    "to copy, modify, sublicense or distribute the Program is void, and\n" +
                    "will automatically terminate your rights under this License. However,\n" +
                    "parties who have received copies, or rights, from you under this\n" +
                    "License will not have their licenses terminated so long as such\n" +
                    "parties remain in full compliance.\n" +
                    "\n" +
                    "**5.** You are not required to accept this License, since you have not\n" +
                    "signed it. However, nothing else grants you permission to modify or\n" +
                    "distribute the Program or its derivative works. These actions are\n" +
                    "prohibited by law if you do not accept this License. Therefore, by\n" +
                    "modifying or distributing the Program (or any work based on the\n" +
                    "Program), you indicate your acceptance of this License to do so, and\n" +
                    "all its terms and conditions for copying, distributing or modifying\n" +
                    "the Program or works based on it.\n" +
                    "\n" +
                    "**6.** Each time you redistribute the Program (or any work based on\n" +
                    "the Program), the recipient automatically receives a license from the\n" +
                    "original licensor to copy, distribute or modify the Program subject to\n" +
                    "these terms and conditions. You may not impose any further\n" +
                    "restrictions on the recipients' exercise of the rights granted herein.\n" +
                    "You are not responsible for enforcing compliance by third parties to\n" +
                    "this License.\n" +
                    "\n" +
                    "**7.** If, as a consequence of a court judgment or allegation of\n" +
                    "patent infringement or for any other reason (not limited to patent\n" +
                    "issues), conditions are imposed on you (whether by court order,\n" +
                    "agreement or otherwise) that contradict the conditions of this\n" +
                    "License, they do not excuse you from the conditions of this License.\n" +
                    "If you cannot distribute so as to satisfy simultaneously your\n" +
                    "obligations under this License and any other pertinent obligations,\n" +
                    "then as a consequence you may not distribute the Program at all. For\n" +
                    "example, if a patent license would not permit royalty-free\n" +
                    "redistribution of the Program by all those who receive copies directly\n" +
                    "or indirectly through you, then the only way you could satisfy both it\n" +
                    "and this License would be to refrain entirely from distribution of the\n" +
                    "Program.\n" +
                    "\n" +
                    "If any portion of this section is held invalid or unenforceable under\n" +
                    "any particular circumstance, the balance of the section is intended to\n" +
                    "apply and the section as a whole is intended to apply in other\n" +
                    "circumstances.\n" +
                    "\n" +
                    "It is not the purpose of this section to induce you to infringe any\n" +
                    "patents or other property right claims or to contest validity of any\n" +
                    "such claims; this section has the sole purpose of protecting the\n" +
                    "integrity of the free software distribution system, which is\n" +
                    "implemented by public license practices. Many people have made\n" +
                    "generous contributions to the wide range of software distributed\n" +
                    "through that system in reliance on consistent application of that\n" +
                    "system; it is up to the author/donor to decide if he or she is willing\n" +
                    "to distribute software through any other system and a licensee cannot\n" +
                    "impose that choice.\n" +
                    "\n" +
                    "This section is intended to make thoroughly clear what is believed to\n" +
                    "be a consequence of the rest of this License.\n" +
                    "\n" +
                    "**8.** If the distribution and/or use of the Program is restricted in\n" +
                    "certain countries either by patents or by copyrighted interfaces, the\n" +
                    "original copyright holder who places the Program under this License\n" +
                    "may add an explicit geographical distribution limitation excluding\n" +
                    "those countries, so that distribution is permitted only in or among\n" +
                    "countries not thus excluded. In such case, this License incorporates\n" +
                    "the limitation as if written in the body of this License.\n" +
                    "\n" +
                    "**9.** The Free Software Foundation may publish revised and/or new\n" +
                    "versions of the General Public License from time to time. Such new\n" +
                    "versions will be similar in spirit to the present version, but may\n" +
                    "differ in detail to address new problems or concerns.\n" +
                    "\n" +
                    "Each version is given a distinguishing version number. If the Program\n" +
                    "specifies a version number of this License which applies to it and\n" +
                    "\"any later version\", you have the option of following the terms and\n" +
                    "conditions either of that version or of any later version published by\n" +
                    "the Free Software Foundation. If the Program does not specify a\n" +
                    "version number of this License, you may choose any version ever\n" +
                    "published by the Free Software Foundation.\n" +
                    "\n" +
                    "**10.** If you wish to incorporate parts of the Program into other\n" +
                    "free programs whose distribution conditions are different, write to\n" +
                    "the author to ask for permission. For software which is copyrighted by\n" +
                    "the Free Software Foundation, write to the Free Software Foundation;\n" +
                    "we sometimes make exceptions for this. Our decision will be guided by\n" +
                    "the two goals of preserving the free status of all derivatives of our\n" +
                    "free software and of promoting the sharing and reuse of software\n" +
                    "generally.\n" +
                    "\n" +
                    "**NO WARRANTY**\n" +
                    "\n" +
                    "**11.** BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO\n" +
                    "WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.\n" +
                    "EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR\n" +
                    "OTHER PARTIES PROVIDE THE PROGRAM \"AS IS\" WITHOUT WARRANTY OF ANY\n" +
                    "KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE\n" +
                    "IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR\n" +
                    "PURPOSE. THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE\n" +
                    "PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME\n" +
                    "THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.\n" +
                    "\n" +
                    "**12.** IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN\n" +
                    "WRITING WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY\n" +
                    "AND/OR REDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU\n" +
                    "FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR\n" +
                    "CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE THE\n" +
                    "PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING\n" +
                    "RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A\n" +
                    "FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS), EVEN IF\n" +
                    "SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH\n" +
                    "DAMAGES.\n" +
                    "\n" +
                    "### END OF TERMS AND CONDITIONS\n" +
                    "\n" +
                    "### How to Apply These Terms to Your New Programs\n" +
                    "\n" +
                    "If you develop a new program, and you want it to be of the greatest\n" +
                    "possible use to the public, the best way to achieve this is to make it\n" +
                    "free software which everyone can redistribute and change under these\n" +
                    "terms.\n" +
                    "\n" +
                    "To do so, attach the following notices to the program. It is safest to\n" +
                    "attach them to the start of each source file to most effectively\n" +
                    "convey the exclusion of warranty; and each file should have at least\n" +
                    "the \"copyright\" line and a pointer to where the full notice is found.\n" +
                    "\n" +
                    "    one line to give the program's name and an idea of what it does.\n" +
                    "    Copyright (C) yyyy  name of author\n" +
                    "\n" +
                    "    This program is free software; you can redistribute it and/or\n" +
                    "    modify it under the terms of the GNU General Public License\n" +
                    "    as published by the Free Software Foundation; either version 2\n" +
                    "    of the License, or (at your option) any later version.\n" +
                    "\n" +
                    "    This program is distributed in the hope that it will be useful,\n" +
                    "    but WITHOUT ANY WARRANTY; without even the implied warranty of\n" +
                    "    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n" +
                    "    GNU General Public License for more details.\n" +
                    "\n" +
                    "    You should have received a copy of the GNU General Public License\n" +
                    "    along with this program; if not, write to the Free Software\n" +
                    "    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.\n" +
                    "\n" +
                    "Also add information on how to contact you by electronic and paper\n" +
                    "mail.\n" +
                    "\n" +
                    "If the program is interactive, make it output a short notice like this\n" +
                    "when it starts in an interactive mode:\n" +
                    "\n" +
                    "    Gnomovision version 69, Copyright (C) year name of author\n" +
                    "    Gnomovision comes with ABSOLUTELY NO WARRANTY; for details\n" +
                    "    type `show w'.  This is free software, and you are welcome\n" +
                    "    to redistribute it under certain conditions; type `show c' \n" +
                    "    for details.\n" +
                    "\n" +
                    "The hypothetical commands \\`show w' and \\`show c' should show the\n" +
                    "appropriate parts of the General Public License. Of course, the\n" +
                    "commands you use may be called something other than \\`show w' and\n" +
                    "\\`show c'; they could even be mouse-clicks or menu items--whatever\n" +
                    "suits your program.\n" +
                    "\n" +
                    "You should also get your employer (if you work as a programmer) or\n" +
                    "your school, if any, to sign a \"copyright disclaimer\" for the program,\n" +
                    "if necessary. Here is a sample; alter the names:\n" +
                    "\n" +
                    "    Yoyodyne, Inc., hereby disclaims all copyright\n" +
                    "    interest in the program `Gnomovision'\n" +
                    "    (which makes passes at compilers) written \n" +
                    "    by James Hacker.\n" +
                    "\n" +
                    "    signature of Ty Coon, 1 April 1989\n" +
                    "    Ty Coon, President of Vice\n" +
                    "\n" +
                    "This General Public License does not permit incorporating your program\n" +
                    "into proprietary programs. If your program is a subroutine library,\n" +
                    "you may consider it more useful to permit linking proprietary\n" +
                    "applications with the library. If this is what you want to do, use the\n" +
                    "[GNU Lesser General Public\n" +
                    "License](https://www.gnu.org/licenses/lgpl.html) instead of this\n" +
                    "License.";
    }

    public static String gplv3() {
        return "GNU GENERAL PUBLIC LICENSE\n" +
                    "~~~~~~~~~~~~~~~~~~~~~~~~~~\n" +
                    "\n" +
                    "Version 3, 29 June 2007\n" +
                    "\n" +
                    "Copyright (C) 2007 Free Software Foundation, Inc. https://fsf.org/\n" +
                    "\n" +
                    "Everyone is permitted to copy and distribute verbatim copies of this\n" +
                    "license document, but changing it is not allowed.\n" +
                    "\n" +
                    "Preamble\n" +
                    "~~~~~~~~\n" +
                    "\n" +
                    "The GNU General Public License is a free, copyleft license for software\n" +
                    "and other kinds of works.\n" +
                    "\n" +
                    "The licenses for most software and other practical works are designed to\n" +
                    "take away your freedom to share and change the works. By contrast, the\n" +
                    "GNU General Public License is intended to guarantee your freedom to\n" +
                    "share and change all versions of a program--to make sure it remains free\n" +
                    "software for all its users. We, the Free Software Foundation, use the\n" +
                    "GNU General Public License for most of our software; it applies also to\n" +
                    "any other work released this way by its authors. You can apply it to\n" +
                    "your programs, too.\n" +
                    "\n" +
                    "When we speak of free software, we are referring to freedom, not price.\n" +
                    "Our General Public Licenses are designed to make sure that you have the\n" +
                    "freedom to distribute copies of free software (and charge for them if\n" +
                    "you wish), that you receive source code or can get it if you want it,\n" +
                    "that you can change the software or use pieces of it in new free\n" +
                    "programs, and that you know you can do these things.\n" +
                    "\n" +
                    "To protect your rights, we need to prevent others from denying you these\n" +
                    "rights or asking you to surrender the rights. Therefore, you have\n" +
                    "certain responsibilities if you distribute copies of the software, or if\n" +
                    "you modify it: responsibilities to respect the freedom of others.\n" +
                    "\n" +
                    "For example, if you distribute copies of such a program, whether gratis\n" +
                    "or for a fee, you must pass on to the recipients the same freedoms that\n" +
                    "you received. You must make sure that they, too, receive or can get the\n" +
                    "source code. And you must show them these terms so they know their\n" +
                    "rights.\n" +
                    "\n" +
                    "Developers that use the GNU GPL protect your rights with two steps: (1)\n" +
                    "assert copyright on the software, and (2) offer you this License giving\n" +
                    "you legal permission to copy, distribute and/or modify it.\n" +
                    "\n" +
                    "For the developers' and authors' protection, the GPL clearly explains\n" +
                    "that there is no warranty for this free software. For both users' and\n" +
                    "authors' sake, the GPL requires that modified versions be marked as\n" +
                    "changed, so that their problems will not be attributed erroneously to\n" +
                    "authors of previous versions.\n" +
                    "\n" +
                    "Some devices are designed to deny users access to install or run\n" +
                    "modified versions of the software inside them, although the manufacturer\n" +
                    "can do so. This is fundamentally incompatible with the aim of protecting\n" +
                    "users' freedom to change the software. The systematic pattern of such\n" +
                    "abuse occurs in the area of products for individuals to use, which is\n" +
                    "precisely where it is most unacceptable. Therefore, we have designed\n" +
                    "this version of the GPL to prohibit the practice for those products. If\n" +
                    "such problems arise substantially in other domains, we stand ready to\n" +
                    "extend this provision to those domains in future versions of the GPL, as\n" +
                    "needed to protect the freedom of users.\n" +
                    "\n" +
                    "Finally, every program is threatened constantly by software patents.\n" +
                    "States should not allow patents to restrict development and use of\n" +
                    "software on general-purpose computers, but in those that do, we wish to\n" +
                    "avoid the special danger that patents applied to a free program could\n" +
                    "make it effectively proprietary. To prevent this, the GPL assures that\n" +
                    "patents cannot be used to render the program non-free.\n" +
                    "\n" +
                    "The precise terms and conditions for copying, distribution and\n" +
                    "modification follow.\n" +
                    "\n" +
                    "TERMS AND CONDITIONS\n" +
                    "~~~~~~~~~~~~~~~~~~~~\n" +
                    "\n" +
                    "0. Definitions.\n" +
                    "^^^^^^^^^^^^^^^\n" +
                    "\n" +
                    "\"This License\" refers to version 3 of the GNU General Public License.\n" +
                    "\n" +
                    "\"Copyright\" also means copyright-like laws that apply to other kinds of\n" +
                    "works, such as semiconductor masks.\n" +
                    "\n" +
                    "\"The Program\" refers to any copyrightable work licensed under this\n" +
                    "License. Each licensee is addressed as \"you\". \"Licensees\" and\n" +
                    "\"recipients\" may be individuals or organizations.\n" +
                    "\n" +
                    "To \"modify\" a work means to copy from or adapt all or part of the work\n" +
                    "in a fashion requiring copyright permission, other than the making of an\n" +
                    "exact copy. The resulting work is called a \"modified version\" of the\n" +
                    "earlier work or a work \"based on\" the earlier work.\n" +
                    "\n" +
                    "A \"covered work\" means either the unmodified Program or a work based on\n" +
                    "the Program.\n" +
                    "\n" +
                    "To \"propagate\" a work means to do anything with it that, without\n" +
                    "permission, would make you directly or secondarily liable for\n" +
                    "infringement under applicable copyright law, except executing it on a\n" +
                    "computer or modifying a private copy. Propagation includes copying,\n" +
                    "distribution (with or without modification), making available to the\n" +
                    "public, and in some countries other activities as well.\n" +
                    "\n" +
                    "To \"convey\" a work means any kind of propagation that enables other\n" +
                    "parties to make or receive copies. Mere interaction with a user through\n" +
                    "a computer network, with no transfer of a copy, is not conveying.\n" +
                    "\n" +
                    "An interactive user interface displays \"Appropriate Legal Notices\" to\n" +
                    "the extent that it includes a convenient and prominently visible feature\n" +
                    "that (1) displays an appropriate copyright notice, and (2) tells the\n" +
                    "user that there is no warranty for the work (except to the extent that\n" +
                    "warranties are provided), that licensees may convey the work under this\n" +
                    "License, and how to view a copy of this License. If the interface\n" +
                    "presents a list of user commands or options, such as a menu, a prominent\n" +
                    "item in the list meets this criterion.\n" +
                    "\n" +
                    "1. Source Code.\n" +
                    "^^^^^^^^^^^^^^^\n" +
                    "\n" +
                    "The \"source code\" for a work means the preferred form of the work for\n" +
                    "making modifications to it. \"Object code\" means any non-source form of a\n" +
                    "work.\n" +
                    "\n" +
                    "A \"Standard Interface\" means an interface that either is an official\n" +
                    "standard defined by a recognized standards body, or, in the case of\n" +
                    "interfaces specified for a particular programming language, one that is\n" +
                    "widely used among developers working in that language.\n" +
                    "\n" +
                    "The \"System Libraries\" of an executable work include anything, other\n" +
                    "than the work as a whole, that (a) is included in the normal form of\n" +
                    "packaging a Major Component, but which is not part of that Major\n" +
                    "Component, and (b) serves only to enable use of the work with that Major\n" +
                    "Component, or to implement a Standard Interface for which an\n" +
                    "implementation is available to the public in source code form. A \"Major\n" +
                    "Component\", in this context, means a major essential component (kernel,\n" +
                    "window system, and so on) of the specific operating system (if any) on\n" +
                    "which the executable work runs, or a compiler used to produce the work,\n" +
                    "or an object code interpreter used to run it.\n" +
                    "\n" +
                    "The \"Corresponding Source\" for a work in object code form means all the\n" +
                    "source code needed to generate, install, and (for an executable work)\n" +
                    "run the object code and to modify the work, including scripts to control\n" +
                    "those activities. However, it does not include the work's System\n" +
                    "Libraries, or general-purpose tools or generally available free programs\n" +
                    "which are used unmodified in performing those activities but which are\n" +
                    "not part of the work. For example, Corresponding Source includes\n" +
                    "interface definition files associated with source files for the work,\n" +
                    "and the source code for shared libraries and dynamically linked\n" +
                    "subprograms that the work is specifically designed to require, such as\n" +
                    "by intimate data communication or control flow between those subprograms\n" +
                    "and other parts of the work.\n" +
                    "\n" +
                    "The Corresponding Source need not include anything that users can\n" +
                    "regenerate automatically from other parts of the Corresponding Source.\n" +
                    "\n" +
                    "The Corresponding Source for a work in source code form is that same\n" +
                    "work.\n" +
                    "\n" +
                    "2. Basic Permissions.\n" +
                    "^^^^^^^^^^^^^^^^^^^^^\n" +
                    "\n" +
                    "All rights granted under this License are granted for the term of\n" +
                    "copyright on the Program, and are irrevocable provided the stated\n" +
                    "conditions are met. This License explicitly affirms your unlimited\n" +
                    "permission to run the unmodified Program. The output from running a\n" +
                    "covered work is covered by this License only if the output, given its\n" +
                    "content, constitutes a covered work. This License acknowledges your\n" +
                    "rights of fair use or other equivalent, as provided by copyright law.\n" +
                    "\n" +
                    "You may make, run and propagate covered works that you do not convey,\n" +
                    "without conditions so long as your license otherwise remains in force.\n" +
                    "You may convey covered works to others for the sole purpose of having\n" +
                    "them make modifications exclusively for you, or provide you with\n" +
                    "facilities for running those works, provided that you comply with the\n" +
                    "terms of this License in conveying all material for which you do not\n" +
                    "control copyright. Those thus making or running the covered works for\n" +
                    "you must do so exclusively on your behalf, under your direction and\n" +
                    "control, on terms that prohibit them from making any copies of your\n" +
                    "copyrighted material outside their relationship with you.\n" +
                    "\n" +
                    "Conveying under any other circumstances is permitted solely under the\n" +
                    "conditions stated below. Sublicensing is not allowed; section 10 makes\n" +
                    "it unnecessary.\n" +
                    "\n" +
                    "3. Protecting Users' Legal Rights From Anti-Circumvention Law.\n" +
                    "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n" +
                    "\n" +
                    "No covered work shall be deemed part of an effective technological\n" +
                    "measure under any applicable law fulfilling obligations under article 11\n" +
                    "of the WIPO copyright treaty adopted on 20 December 1996, or similar\n" +
                    "laws prohibiting or restricting circumvention of such measures.\n" +
                    "\n" +
                    "When you convey a covered work, you waive any legal power to forbid\n" +
                    "circumvention of technological measures to the extent such circumvention\n" +
                    "is effected by exercising rights under this License with respect to the\n" +
                    "covered work, and you disclaim any intention to limit operation or\n" +
                    "modification of the work as a means of enforcing, against the work's\n" +
                    "users, your or third parties' legal rights to forbid circumvention of\n" +
                    "technological measures.\n" +
                    "\n" +
                    "4. Conveying Verbatim Copies.\n" +
                    "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n" +
                    "\n" +
                    "You may convey verbatim copies of the Program's source code as you\n" +
                    "receive it, in any medium, provided that you conspicuously and\n" +
                    "appropriately publish on each copy an appropriate copyright notice; keep\n" +
                    "intact all notices stating that this License and any non-permissive\n" +
                    "terms added in accord with section 7 apply to the code; keep intact all\n" +
                    "notices of the absence of any warranty; and give all recipients a copy\n" +
                    "of this License along with the Program.\n" +
                    "\n" +
                    "You may charge any price or no price for each copy that you convey, and\n" +
                    "you may offer support or warranty protection for a fee.\n" +
                    "\n" +
                    "5. Conveying Modified Source Versions.\n" +
                    "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n" +
                    "\n" +
                    "You may convey a work based on the Program, or the modifications to\n" +
                    "produce it from the Program, in the form of source code under the terms\n" +
                    "of section 4, provided that you also meet all of these conditions:\n" +
                    "\n" +
                    "a) The work must carry prominent notices stating that you modified\n" +
                    "   it, and giving a relevant date.\n" +
                    "\n" +
                    "b) The work must carry prominent notices stating that it is released\n" +
                    "   under this License and any conditions added under section 7. This\n" +
                    "   requirement modifies the requirement in section 4 to \"keep intact\n" +
                    "   all notices\".\n" +
                    "\n" +
                    "c) You must license the entire work, as a whole, under this License\n" +
                    "   to anyone who comes into possession of a copy. This License will\n" +
                    "   therefore apply, along with any applicable section 7 additional\n" +
                    "   terms, to the whole of the work, and all its parts, regardless of\n" +
                    "   how they are packaged. This License gives no permission to license\n" +
                    "   the work in any other way, but it does not invalidate such\n" +
                    "   permission if you have separately received it.\n" +
                    "\n" +
                    "d) If the work has interactive user interfaces, each must display\n" +
                    "   Appropriate Legal Notices; however, if the Program has interactive\n" +
                    "   interfaces that do not display Appropriate Legal Notices, your\n" +
                    "   work need not make them do so.\n" +
                    "\n" +
                    "A compilation of a covered work with other separate and independent\n" +
                    "works, which are not by their nature extensions of the covered work, and\n" +
                    "which are not combined with it such as to form a larger program, in or\n" +
                    "on a volume of a storage or distribution medium, is called an\n" +
                    "\"aggregate\" if the compilation and its resulting copyright are not used\n" +
                    "to limit the access or legal rights of the compilation's users beyond\n" +
                    "what the individual works permit. Inclusion of a covered work in an\n" +
                    "aggregate does not cause this License to apply to the other parts of the\n" +
                    "aggregate.\n" +
                    "\n" +
                    "6. Conveying Non-Source Forms.\n" +
                    "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n" +
                    "\n" +
                    "You may convey a covered work in object code form under the terms of\n" +
                    "sections 4 and 5, provided that you also convey the machine-readable\n" +
                    "Corresponding Source under the terms of this License, in one of these\n" +
                    "ways:\n" +
                    "\n" +
                    "a) Convey the object code in, or embodied in, a physical product\n" +
                    "   (including a physical distribution medium), accompanied by the\n" +
                    "   Corresponding Source fixed on a durable physical medium\n" +
                    "   customarily used for software interchange.\n" +
                    "\n" +
                    "b) Convey the object code in, or embodied in, a physical product\n" +
                    "   (including a physical distribution medium), accompanied by a\n" +
                    "   written offer, valid for at least three years and valid for as\n" +
                    "   long as you offer spare parts or customer support for that product\n" +
                    "   model, to give anyone who possesses the object code either (1) a\n" +
                    "   copy of the Corresponding Source for all the software in the\n" +
                    "   product that is covered by this License, on a durable physical\n" +
                    "   medium customarily used for software interchange, for a price no\n" +
                    "   more than your reasonable cost of physically performing this\n" +
                    "   conveying of source, or (2) access to copy the Corresponding\n" +
                    "   Source from a network server at no charge.\n" +
                    "\n" +
                    "c) Convey individual copies of the object code with a copy of the\n" +
                    "   written offer to provide the Corresponding Source. This\n" +
                    "   alternative is allowed only occasionally and noncommercially, and\n" +
                    "   only if you received the object code with such an offer, in accord\n" +
                    "   with subsection 6b.\n" +
                    "\n" +
                    "d) Convey the object code by offering access from a designated place\n" +
                    "   (gratis or for a charge), and offer equivalent access to the\n" +
                    "   Corresponding Source in the same way through the same place at no\n" +
                    "   further charge. You need not require recipients to copy the\n" +
                    "   Corresponding Source along with the object code. If the place to\n" +
                    "   copy the object code is a network server, the Corresponding Source\n" +
                    "   may be on a different server (operated by you or a third party)\n" +
                    "   that supports equivalent copying facilities, provided you maintain\n" +
                    "   clear directions next to the object code saying where to find the\n" +
                    "   Corresponding Source. Regardless of what server hosts the\n" +
                    "   Corresponding Source, you remain obligated to ensure that it is\n" +
                    "   available for as long as needed to satisfy these requirements.\n" +
                    "\n" +
                    "e) Convey the object code using peer-to-peer transmission, provided\n" +
                    "   you inform other peers where the object code and Corresponding\n" +
                    "   Source of the work are being offered to the general public at no\n" +
                    "   charge under subsection 6d.\n" +
                    "\n" +
                    "A separable portion of the object code, whose source code is excluded\n" +
                    "from the Corresponding Source as a System Library, need not be included\n" +
                    "in conveying the object code work.\n" +
                    "\n" +
                    "A \"User Product\" is either (1) a \"consumer product\", which means any\n" +
                    "tangible personal property which is normally used for personal, family,\n" +
                    "or household purposes, or (2) anything designed or sold for\n" +
                    "incorporation into a dwelling. In determining whether a product is a\n" +
                    "consumer product, doubtful cases shall be resolved in favor of coverage.\n" +
                    "For a particular product received by a particular user, \"normally used\"\n" +
                    "refers to a typical or common use of that class of product, regardless\n" +
                    "of the status of the particular user or of the way in which the\n" +
                    "particular user actually uses, or expects or is expected to use, the\n" +
                    "product. A product is a consumer product regardless of whether the\n" +
                    "product has substantial commercial, industrial or non-consumer uses,\n" +
                    "unless such uses represent the only significant mode of use of the\n" +
                    "product.\n" +
                    "\n" +
                    "\"Installation Information\" for a User Product means any methods,\n" +
                    "procedures, authorization keys, or other information required to install\n" +
                    "and execute modified versions of a covered work in that User Product\n" +
                    "from a modified version of its Corresponding Source. The information\n" +
                    "must suffice to ensure that the continued functioning of the modified\n" +
                    "object code is in no case prevented or interfered with solely because\n" +
                    "modification has been made.\n" +
                    "\n" +
                    "If you convey an object code work under this section in, or with, or\n" +
                    "specifically for use in, a User Product, and the conveying occurs as\n" +
                    "part of a transaction in which the right of possession and use of the\n" +
                    "User Product is transferred to the recipient in perpetuity or for a\n" +
                    "fixed term (regardless of how the transaction is characterized), the\n" +
                    "Corresponding Source conveyed under this section must be accompanied by\n" +
                    "the Installation Information. But this requirement does not apply if\n" +
                    "neither you nor any third party retains the ability to install modified\n" +
                    "object code on the User Product (for example, the work has been\n" +
                    "installed in ROM).\n" +
                    "\n" +
                    "The requirement to provide Installation Information does not include a\n" +
                    "requirement to continue to provide support service, warranty, or updates\n" +
                    "for a work that has been modified or installed by the recipient, or for\n" +
                    "the User Product in which it has been modified or installed. Access to a\n" +
                    "network may be denied when the modification itself materially and\n" +
                    "adversely affects the operation of the network or violates the rules and\n" +
                    "protocols for communication across the network.\n" +
                    "\n" +
                    "Corresponding Source conveyed, and Installation Information provided, in\n" +
                    "accord with this section must be in a format that is publicly documented\n" +
                    "(and with an implementation available to the public in source code\n" +
                    "form), and must require no special password or key for unpacking,\n" +
                    "reading or copying.\n" +
                    "\n" +
                    "7. Additional Terms.\n" +
                    "^^^^^^^^^^^^^^^^^^^^\n" +
                    "\n" +
                    "\"Additional permissions\" are terms that supplement the terms of this\n" +
                    "License by making exceptions from one or more of its conditions.\n" +
                    "Additional permissions that are applicable to the entire Program shall\n" +
                    "be treated as though they were included in this License, to the extent\n" +
                    "that they are valid under applicable law. If additional permissions\n" +
                    "apply only to part of the Program, that part may be used separately\n" +
                    "under those permissions, but the entire Program remains governed by this\n" +
                    "License without regard to the additional permissions.\n" +
                    "\n" +
                    "When you convey a copy of a covered work, you may at your option remove\n" +
                    "any additional permissions from that copy, or from any part of it.\n" +
                    "(Additional permissions may be written to require their own removal in\n" +
                    "certain cases when you modify the work.) You may place additional\n" +
                    "permissions on material, added by you to a covered work, for which you\n" +
                    "have or can give appropriate copyright permission.\n" +
                    "\n" +
                    "Notwithstanding any other provision of this License, for material you\n" +
                    "add to a covered work, you may (if authorized by the copyright holders\n" +
                    "of that material) supplement the terms of this License with terms:\n" +
                    "\n" +
                    "a) Disclaiming warranty or limiting liability differently from the\n" +
                    "   terms of sections 15 and 16 of this License; or\n" +
                    "\n" +
                    "b) Requiring preservation of specified reasonable legal notices or\n" +
                    "   author attributions in that material or in the Appropriate Legal\n" +
                    "   Notices displayed by works containing it; or\n" +
                    "\n" +
                    "c) Prohibiting misrepresentation of the origin of that material, or\n" +
                    "   requiring that modified versions of such material be marked in\n" +
                    "   reasonable ways as different from the original version; or\n" +
                    "\n" +
                    "d) Limiting the use for publicity purposes of names of licensors or\n" +
                    "   authors of the material; or\n" +
                    "\n" +
                    "e) Declining to grant rights under trademark law for use of some\n" +
                    "   trade names, trademarks, or service marks; or\n" +
                    "\n" +
                    "f) Requiring indemnification of licensors and authors of that\n" +
                    "   material by anyone who conveys the material (or modified versions\n" +
                    "   of it) with contractual assumptions of liability to the recipient,\n" +
                    "   for any liability that these contractual assumptions directly\n" +
                    "   impose on those licensors and authors.\n" +
                    "\n" +
                    "All other non-permissive additional terms are considered \"further\n" +
                    "restrictions\" within the meaning of section 10. If the Program as you\n" +
                    "received it, or any part of it, contains a notice stating that it is\n" +
                    "governed by this License along with a term that is a further\n" +
                    "restriction, you may remove that term. If a license document contains a\n" +
                    "further restriction but permits relicensing or conveying under this\n" +
                    "License, you may add to a covered work material governed by the terms of\n" +
                    "that license document, provided that the further restriction does not\n" +
                    "survive such relicensing or conveying.\n" +
                    "\n" +
                    "If you add terms to a covered work in accord with this section, you must\n" +
                    "place, in the relevant source files, a statement of the additional terms\n" +
                    "that apply to those files, or a notice indicating where to find the\n" +
                    "applicable terms.\n" +
                    "\n" +
                    "Additional terms, permissive or non-permissive, may be stated in the\n" +
                    "form of a separately written license, or stated as exceptions; the above\n" +
                    "requirements apply either way.\n" +
                    "\n" +
                    "8. Termination.\n" +
                    "^^^^^^^^^^^^^^^\n" +
                    "\n" +
                    "You may not propagate or modify a covered work except as expressly\n" +
                    "provided under this License. Any attempt otherwise to propagate or\n" +
                    "modify it is void, and will automatically terminate your rights under\n" +
                    "this License (including any patent licenses granted under the third\n" +
                    "paragraph of section 11).\n" +
                    "\n" +
                    "However, if you cease all violation of this License, then your license\n" +
                    "from a particular copyright holder is reinstated (a) provisionally,\n" +
                    "unless and until the copyright holder explicitly and finally terminates\n" +
                    "your license, and (b) permanently, if the copyright holder fails to\n" +
                    "notify you of the violation by some reasonable means prior to 60 days\n" +
                    "after the cessation.\n" +
                    "\n" +
                    "Moreover, your license from a particular copyright holder is reinstated\n" +
                    "permanently if the copyright holder notifies you of the violation by\n" +
                    "some reasonable means, this is the first time you have received notice\n" +
                    "of violation of this License (for any work) from that copyright holder,\n" +
                    "and you cure the violation prior to 30 days after your receipt of the\n" +
                    "notice.\n" +
                    "\n" +
                    "Termination of your rights under this section does not terminate the\n" +
                    "licenses of parties who have received copies or rights from you under\n" +
                    "this License. If your rights have been terminated and not permanently\n" +
                    "reinstated, you do not qualify to receive new licenses for the same\n" +
                    "material under section 10.\n" +
                    "\n" +
                    "9. Acceptance Not Required for Having Copies.\n" +
                    "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n" +
                    "\n" +
                    "You are not required to accept this License in order to receive or run a\n" +
                    "copy of the Program. Ancillary propagation of a covered work occurring\n" +
                    "solely as a consequence of using peer-to-peer transmission to receive a\n" +
                    "copy likewise does not require acceptance. However, nothing other than\n" +
                    "this License grants you permission to propagate or modify any covered\n" +
                    "work. These actions infringe copyright if you do not accept this\n" +
                    "License. Therefore, by modifying or propagating a covered work, you\n" +
                    "indicate your acceptance of this License to do so.\n" +
                    "\n" +
                    "10. Automatic Licensing of Downstream Recipients.\n" +
                    "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n" +
                    "\n" +
                    "Each time you convey a covered work, the recipient automatically\n" +
                    "receives a license from the original licensors, to run, modify and\n" +
                    "propagate that work, subject to this License. You are not responsible\n" +
                    "for enforcing compliance by third parties with this License.\n" +
                    "\n" +
                    "An \"entity transaction\" is a transaction transferring control of an\n" +
                    "organization, or substantially all assets of one, or subdividing an\n" +
                    "organization, or merging organizations. If propagation of a covered work\n" +
                    "results from an entity transaction, each party to that transaction who\n" +
                    "receives a copy of the work also receives whatever licenses to the work\n" +
                    "the party's predecessor in interest had or could give under the previous\n" +
                    "paragraph, plus a right to possession of the Corresponding Source of the\n" +
                    "work from the predecessor in interest, if the predecessor has it or can\n" +
                    "get it with reasonable efforts.\n" +
                    "\n" +
                    "You may not impose any further restrictions on the exercise of the\n" +
                    "rights granted or affirmed under this License. For example, you may not\n" +
                    "impose a license fee, royalty, or other charge for exercise of rights\n" +
                    "granted under this License, and you may not initiate litigation\n" +
                    "(including a cross-claim or counterclaim in a lawsuit) alleging that any\n" +
                    "patent claim is infringed by making, using, selling, offering for sale,\n" +
                    "or importing the Program or any portion of it.\n" +
                    "\n" +
                    "11. Patents.\n" +
                    "^^^^^^^^^^^^\n" +
                    "\n" +
                    "A \"contributor\" is a copyright holder who authorizes use under this\n" +
                    "License of the Program or a work on which the Program is based. The work\n" +
                    "thus licensed is called the contributor's \"contributor version\".\n" +
                    "\n" +
                    "A contributor's \"essential patent claims\" are all patent claims owned or\n" +
                    "controlled by the contributor, whether already acquired or hereafter\n" +
                    "acquired, that would be infringed by some manner, permitted by this\n" +
                    "License, of making, using, or selling its contributor version, but do\n" +
                    "not include claims that would be infringed only as a consequence of\n" +
                    "further modification of the contributor version. For purposes of this\n" +
                    "definition, \"control\" includes the right to grant patent sublicenses in\n" +
                    "a manner consistent with the requirements of this License.\n" +
                    "\n" +
                    "Each contributor grants you a non-exclusive, worldwide, royalty-free\n" +
                    "patent license under the contributor's essential patent claims, to make,\n" +
                    "use, sell, offer for sale, import and otherwise run, modify and\n" +
                    "propagate the contents of its contributor version.\n" +
                    "\n" +
                    "In the following three paragraphs, a \"patent license\" is any express\n" +
                    "agreement or commitment, however denominated, not to enforce a patent\n" +
                    "(such as an express permission to practice a patent or covenant not to\n" +
                    "sue for patent infringement). To \"grant\" such a patent license to a\n" +
                    "party means to make such an agreement or commitment not to enforce a\n" +
                    "patent against the party.\n" +
                    "\n" +
                    "If you convey a covered work, knowingly relying on a patent license, and\n" +
                    "the Corresponding Source of the work is not available for anyone to\n" +
                    "copy, free of charge and under the terms of this License, through a\n" +
                    "publicly available network server or other readily accessible means,\n" +
                    "then you must either (1) cause the Corresponding Source to be so\n" +
                    "available, or (2) arrange to deprive yourself of the benefit of the\n" +
                    "patent license for this particular work, or (3) arrange, in a manner\n" +
                    "consistent with the requirements of this License, to extend the patent\n" +
                    "license to downstream recipients. \"Knowingly relying\" means you have\n" +
                    "actual knowledge that, but for the patent license, your conveying the\n" +
                    "covered work in a country, or your recipient's use of the covered work\n" +
                    "in a country, would infringe one or more identifiable patents in that\n" +
                    "country that you have reason to believe are valid.\n" +
                    "\n" +
                    "If, pursuant to or in connection with a single transaction or\n" +
                    "arrangement, you convey, or propagate by procuring conveyance of, a\n" +
                    "covered work, and grant a patent license to some of the parties\n" +
                    "receiving the covered work authorizing them to use, propagate, modify or\n" +
                    "convey a specific copy of the covered work, then the patent license you\n" +
                    "grant is automatically extended to all recipients of the covered work\n" +
                    "and works based on it.\n" +
                    "\n" +
                    "A patent license is \"discriminatory\" if it does not include within the\n" +
                    "scope of its coverage, prohibits the exercise of, or is conditioned on\n" +
                    "the non-exercise of one or more of the rights that are specifically\n" +
                    "granted under this License. You may not convey a covered work if you are\n" +
                    "a party to an arrangement with a third party that is in the business of\n" +
                    "distributing software, under which you make payment to the third party\n" +
                    "based on the extent of your activity of conveying the work, and under\n" +
                    "which the third party grants, to any of the parties who would receive\n" +
                    "the covered work from you, a discriminatory patent license (a) in\n" +
                    "connection with copies of the covered work conveyed by you (or copies\n" +
                    "made from those copies), or (b) primarily for and in connection with\n" +
                    "specific products or compilations that contain the covered work, unless\n" +
                    "you entered into that arrangement, or that patent license was granted,\n" +
                    "prior to 28 March 2007.\n" +
                    "\n" +
                    "Nothing in this License shall be construed as excluding or limiting any\n" +
                    "implied license or other defenses to infringement that may otherwise be\n" +
                    "available to you under applicable patent law.\n" +
                    "\n" +
                    "12. No Surrender of Others' Freedom.\n" +
                    "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n" +
                    "\n" +
                    "If conditions are imposed on you (whether by court order, agreement or\n" +
                    "otherwise) that contradict the conditions of this License, they do not\n" +
                    "excuse you from the conditions of this License. If you cannot convey a\n" +
                    "covered work so as to satisfy simultaneously your obligations under this\n" +
                    "License and any other pertinent obligations, then as a consequence you\n" +
                    "may not convey it at all. For example, if you agree to terms that\n" +
                    "obligate you to collect a royalty for further conveying from those to\n" +
                    "whom you convey the Program, the only way you could satisfy both those\n" +
                    "terms and this License would be to refrain entirely from conveying the\n" +
                    "Program.\n" +
                    "\n" +
                    "13. Use with the GNU Affero General Public License.\n" +
                    "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n" +
                    "\n" +
                    "Notwithstanding any other provision of this License, you have permission\n" +
                    "to link or combine any covered work with a work licensed under version 3\n" +
                    "of the GNU Affero General Public License into a single combined work,\n" +
                    "and to convey the resulting work. The terms of this License will\n" +
                    "continue to apply to the part which is the covered work, but the special\n" +
                    "requirements of the GNU Affero General Public License, section 13,\n" +
                    "concerning interaction through a network will apply to the combination\n" +
                    "as such.\n" +
                    "\n" +
                    "14. Revised Versions of this License.\n" +
                    "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n" +
                    "\n" +
                    "The Free Software Foundation may publish revised and/or new versions of\n" +
                    "the GNU General Public License from time to time. Such new versions will\n" +
                    "be similar in spirit to the present version, but may differ in detail to\n" +
                    "address new problems or concerns.\n" +
                    "\n" +
                    "Each version is given a distinguishing version number. If the Program\n" +
                    "specifies that a certain numbered version of the GNU General Public\n" +
                    "License \"or any later version\" applies to it, you have the option of\n" +
                    "following the terms and conditions either of that numbered version or of\n" +
                    "any later version published by the Free Software Foundation. If the\n" +
                    "Program does not specify a version number of the GNU General Public\n" +
                    "License, you may choose any version ever published by the Free Software\n" +
                    "Foundation.\n" +
                    "\n" +
                    "If the Program specifies that a proxy can decide which future versions\n" +
                    "of the GNU General Public License can be used, that proxy's public\n" +
                    "statement of acceptance of a version permanently authorizes you to\n" +
                    "choose that version for the Program.\n" +
                    "\n" +
                    "Later license versions may give you additional or different permissions.\n" +
                    "However, no additional obligations are imposed on any author or\n" +
                    "copyright holder as a result of your choosing to follow a later version.\n" +
                    "\n" +
                    "15. Disclaimer of Warranty.\n" +
                    "^^^^^^^^^^^^^^^^^^^^^^^^^^^\n" +
                    "\n" +
                    "THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY\n" +
                    "APPLICABLE LAW. EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT\n" +
                    "HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM \"AS IS\" WITHOUT\n" +
                    "WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT\n" +
                    "LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A\n" +
                    "PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF\n" +
                    "THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME\n" +
                    "THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.\n" +
                    "\n" +
                    "16. Limitation of Liability.\n" +
                    "^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n" +
                    "\n" +
                    "IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING\n" +
                    "WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR\n" +
                    "CONVEYS THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,\n" +
                    "INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES\n" +
                    "ARISING OUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT\n" +
                    "NOT LIMITED TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES\n" +
                    "SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE\n" +
                    "WITH ANY OTHER PROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN\n" +
                    "ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.\n" +
                    "\n" +
                    "17. Interpretation of Sections 15 and 16.\n" +
                    "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n" +
                    "\n" +
                    "If the disclaimer of warranty and limitation of liability provided above\n" +
                    "cannot be given local legal effect according to their terms, reviewing\n" +
                    "courts shall apply local law that most closely approximates an absolute\n" +
                    "waiver of all civil liability in connection with the Program, unless a\n" +
                    "warranty or assumption of liability accompanies a copy of the Program in\n" +
                    "return for a fee.\n" +
                    "\n" +
                    "END OF TERMS AND CONDITIONS\n" +
                    "\n" +
                    "How to Apply These Terms to Your New Programs\n" +
                    "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n" +
                    "\n" +
                    "If you develop a new program, and you want it to be of the greatest\n" +
                    "possible use to the public, the best way to achieve this is to make it\n" +
                    "free software which everyone can redistribute and change under these\n" +
                    "terms.\n" +
                    "\n" +
                    "To do so, attach the following notices to the program. It is safest to\n" +
                    "attach them to the start of each source file to most effectively state\n" +
                    "the exclusion of warranty; and each file should have at least the\n" +
                    "\"copyright\" line and a pointer to where the full notice is found.\n" +
                    "\n" +
                    "::\n" +
                    "\n" +
                    "        <one line to give the program's name and a brief idea of what it does.>\n" +
                    "        Copyright (C) <year>  <name of author>\n" +
                    " \n" +
                    "        This program is free software: you can redistribute it and/or modify\n" +
                    "        it under the terms of the GNU General Public License as published by\n" +
                    "        the Free Software Foundation, either version 3 of the License, or\n" +
                    "        (at your option) any later version.\n" +
                    " \n" +
                    "        This program is distributed in the hope that it will be useful,\n" +
                    "        but WITHOUT ANY WARRANTY; without even the implied warranty of\n" +
                    "        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n" +
                    "        GNU General Public License for more details.\n" +
                    " \n" +
                    "        You should have received a copy of the GNU General Public License\n" +
                    "        along with this program.  If not, see <https://www.gnu.org/licenses/>.\n" +
                    "\n" +
                    "Also add information on how to contact you by electronic and paper mail.\n" +
                    "\n" +
                    "If the program does terminal interaction, make it output a short notice\n" +
                    "like this when it starts in an interactive mode:\n" +
                    "\n" +
                    "::\n" +
                    "\n" +
                    "        <program>  Copyright (C) <year>  <name of author>\n" +
                    "        This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.\n" +
                    "        This is free software, and you are welcome to redistribute it\n" +
                    "        under certain conditions; type `show c' for details.\n" +
                    "\n" +
                    "The hypothetical commands \\`show w' and \\`show c' should show the\n" +
                    "appropriate parts of the General Public License. Of course, your\n" +
                    "program's commands might be different; for a GUI interface, you would\n" +
                    "use an \"about box\".\n" +
                    "\n" +
                    "You should also get your employer (if you work as a programmer) or\n" +
                    "school, if any, to sign a \"copyright disclaimer\" for the program, if\n" +
                    "necessary. For more information on this, and how to apply and follow the\n" +
                    "GNU GPL, see https://www.gnu.org/licenses/.\n" +
                    "\n" +
                    "The GNU General Public License does not permit incorporating your\n" +
                    "program into proprietary programs. If your program is a subroutine\n" +
                    "library, you may consider it more useful to permit linking proprietary\n" +
                    "applications with the library. If this is what you want to do, use the\n" +
                    "GNU Lesser General Public License instead of this License. But first,\n" +
                    "please read https://www.gnu.org/licenses/why-not-lgpl.html.";
    }

    public static String cc_by_sa_3() {
        return "Creative Commons\n" +
                    "\n" +
                    "Attribution-ShareAlike 3.0 Unported\n" +
                    "\n" +
                    "CREATIVE COMMONS CORPORATION IS NOT A LAW FIRM AND DOES NOT PROVIDE LEGAL SERVICES. DISTRIBUTION OF THIS LICENSE DOES NOT CREATE AN ATTORNEY-CLIENT RELATIONSHIP. CREATIVE COMMONS PROVIDES THIS INFORMATION ON AN \"AS-IS\" BASIS. CREATIVE COMMONS MAKES NO WARRANTIES REGARDING THE INFORMATION PROVIDED, AND DISCLAIMS LIABILITY FOR DAMAGES RESULTING FROM ITS USE.\n" +
                    "License\n" +
                    "THE WORK (AS DEFINED BELOW) IS PROVIDED UNDER THE TERMS OF THIS CREATIVE COMMONS PUBLIC LICENSE (\"CCPL\" OR \"LICENSE\"). THE WORK IS PROTECTED BY COPYRIGHT AND/OR OTHER APPLICABLE LAW. ANY USE OF THE WORK OTHER THAN AS AUTHORIZED UNDER THIS LICENSE OR COPYRIGHT LAW IS PROHIBITED.\n" +
                    "\n" +
                    "BY EXERCISING ANY RIGHTS TO THE WORK PROVIDED HERE, YOU ACCEPT AND AGREE TO BE BOUND BY THE TERMS OF THIS LICENSE. TO THE EXTENT THIS LICENSE MAY BE CONSIDERED TO BE A CONTRACT, THE LICENSOR GRANTS YOU THE RIGHTS CONTAINED HERE IN CONSIDERATION OF YOUR ACCEPTANCE OF SUCH TERMS AND CONDITIONS.\n" +
                    "\n" +
                    "1. Definitions\n" +
                    "\n" +
                    "\"Adaptation\" means a work based upon the Work, or upon the Work and other pre-existing works, such as a translation, adaptation, derivative work, arrangement of music or other alterations of a literary or artistic work, or phonogram or performance and includes cinematographic adaptations or any other form in which the Work may be recast, transformed, or adapted including in any form recognizably derived from the original, except that a work that constitutes a Collection will not be considered an Adaptation for the purpose of this License. For the avoidance of doubt, where the Work is a musical work, performance or phonogram, the synchronization of the Work in timed-relation with a moving image (\"synching\") will be considered an Adaptation for the purpose of this License.\n" +
                    "\"Collection\" means a collection of literary or artistic works, such as encyclopedias and anthologies, or performances, phonograms or broadcasts, or other works or subject matter other than works listed in Section 1(f) below, which, by reason of the selection and arrangement of their contents, constitute intellectual creations, in which the Work is included in its entirety in unmodified form along with one or more other contributions, each constituting separate and independent works in themselves, which together are assembled into a collective whole. A work that constitutes a Collection will not be considered an Adaptation (as defined below) for the purposes of this License.\n" +
                    "\"Creative Commons Compatible License\" means a license that is listed at https://creativecommons.org/compatiblelicenses that has been approved by Creative Commons as being essentially equivalent to this License, including, at a minimum, because that license: (i) contains terms that have the same purpose, meaning and effect as the License Elements of this License; and, (ii) explicitly permits the relicensing of adaptations of works made available under that license under this License or a Creative Commons jurisdiction license with the same License Elements as this License.\n" +
                    "\"Distribute\" means to make available to the public the original and copies of the Work or Adaptation, as appropriate, through sale or other transfer of ownership.\n" +
                    "\"License Elements\" means the following high-level license attributes as selected by Licensor and indicated in the title of this License: Attribution, ShareAlike.\n" +
                    "\"Licensor\" means the individual, individuals, entity or entities that offer(s) the Work under the terms of this License.\n" +
                    "\"Original Author\" means, in the case of a literary or artistic work, the individual, individuals, entity or entities who created the Work or if no individual or entity can be identified, the publisher; and in addition (i) in the case of a performance the actors, singers, musicians, dancers, and other persons who act, sing, deliver, declaim, play in, interpret or otherwise perform literary or artistic works or expressions of folklore; (ii) in the case of a phonogram the producer being the person or legal entity who first fixes the sounds of a performance or other sounds; and, (iii) in the case of broadcasts, the organization that transmits the broadcast.\n" +
                    "\"Work\" means the literary and/or artistic work offered under the terms of this License including without limitation any production in the literary, scientific and artistic domain, whatever may be the mode or form of its expression including digital form, such as a book, pamphlet and other writing; a lecture, address, sermon or other work of the same nature; a dramatic or dramatico-musical work; a choreographic work or entertainment in dumb show; a musical composition with or without words; a cinematographic work to which are assimilated works expressed by a process analogous to cinematography; a work of drawing, painting, architecture, sculpture, engraving or lithography; a photographic work to which are assimilated works expressed by a process analogous to photography; a work of applied art; an illustration, map, plan, sketch or three-dimensional work relative to geography, topography, architecture or science; a performance; a broadcast; a phonogram; a compilation of data to the extent it is protected as a copyrightable work; or a work performed by a variety or circus performer to the extent it is not otherwise considered a literary or artistic work.\n" +
                    "\"You\" means an individual or entity exercising rights under this License who has not previously violated the terms of this License with respect to the Work, or who has received express permission from the Licensor to exercise rights under this License despite a previous violation.\n" +
                    "\"Publicly Perform\" means to perform public recitations of the Work and to communicate to the public those public recitations, by any means or process, including by wire or wireless means or public digital performances; to make available to the public Works in such a way that members of the public may access these Works from a place and at a place individually chosen by them; to perform the Work to the public by any means or process and the communication to the public of the performances of the Work, including by public digital performance; to broadcast and rebroadcast the Work by any means including signs, sounds or images.\n" +
                    "\"Reproduce\" means to make copies of the Work by any means including without limitation by sound or visual recordings and the right of fixation and reproducing fixations of the Work, including storage of a protected performance or phonogram in digital form or other electronic medium.\n" +
                    "2. Fair Dealing Rights. Nothing in this License is intended to reduce, limit, or restrict any uses free from copyright or rights arising from limitations or exceptions that are provided for in connection with the copyright protection under copyright law or other applicable laws.\n" +
                    "\n" +
                    "3. License Grant. Subject to the terms and conditions of this License, Licensor hereby grants You a worldwide, royalty-free, non-exclusive, perpetual (for the duration of the applicable copyright) license to exercise the rights in the Work as stated below:\n" +
                    "\n" +
                    "to Reproduce the Work, to incorporate the Work into one or more Collections, and to Reproduce the Work as incorporated in the Collections;\n" +
                    "to create and Reproduce Adaptations provided that any such Adaptation, including any translation in any medium, takes reasonable steps to clearly label, demarcate or otherwise identify that changes were made to the original Work. For example, a translation could be marked \"The original work was translated from English to Spanish,\" or a modification could indicate \"The original work has been modified.\";\n" +
                    "to Distribute and Publicly Perform the Work including as incorporated in Collections; and,\n" +
                    "to Distribute and Publicly Perform Adaptations.\n" +
                    "For the avoidance of doubt:\n" +
                    "\n" +
                    "Non-waivable Compulsory License Schemes. In those jurisdictions in which the right to collect royalties through any statutory or compulsory licensing scheme cannot be waived, the Licensor reserves the exclusive right to collect such royalties for any exercise by You of the rights granted under this License;\n" +
                    "Waivable Compulsory License Schemes. In those jurisdictions in which the right to collect royalties through any statutory or compulsory licensing scheme can be waived, the Licensor waives the exclusive right to collect such royalties for any exercise by You of the rights granted under this License; and,\n" +
                    "Voluntary License Schemes. The Licensor waives the right to collect royalties, whether individually or, in the event that the Licensor is a member of a collecting society that administers voluntary licensing schemes, via that society, from any exercise by You of the rights granted under this License.\n" +
                    "The above rights may be exercised in all media and formats whether now known or hereafter devised. The above rights include the right to make such modifications as are technically necessary to exercise the rights in other media and formats. Subject to Section 8(f), all rights not expressly granted by Licensor are hereby reserved.\n" +
                    "\n" +
                    "4. Restrictions. The license granted in Section 3 above is expressly made subject to and limited by the following restrictions:\n" +
                    "\n" +
                    "You may Distribute or Publicly Perform the Work only under the terms of this License. You must include a copy of, or the Uniform Resource Identifier (URI) for, this License with every copy of the Work You Distribute or Publicly Perform. You may not offer or impose any terms on the Work that restrict the terms of this License or the ability of the recipient of the Work to exercise the rights granted to that recipient under the terms of the License. You may not sublicense the Work. You must keep intact all notices that refer to this License and to the disclaimer of warranties with every copy of the Work You Distribute or Publicly Perform. When You Distribute or Publicly Perform the Work, You may not impose any effective technological measures on the Work that restrict the ability of a recipient of the Work from You to exercise the rights granted to that recipient under the terms of the License. This Section 4(a) applies to the Work as incorporated in a Collection, but this does not require the Collection apart from the Work itself to be made subject to the terms of this License. If You create a Collection, upon notice from any Licensor You must, to the extent practicable, remove from the Collection any credit as required by Section 4(c), as requested. If You create an Adaptation, upon notice from any Licensor You must, to the extent practicable, remove from the Adaptation any credit as required by Section 4(c), as requested.\n" +
                    "You may Distribute or Publicly Perform an Adaptation only under the terms of: (i) this License; (ii) a later version of this License with the same License Elements as this License; (iii) a Creative Commons jurisdiction license (either this or a later license version) that contains the same License Elements as this License (e.g., Attribution-ShareAlike 3.0 US)); (iv) a Creative Commons Compatible License. If you license the Adaptation under one of the licenses mentioned in (iv), you must comply with the terms of that license. If you license the Adaptation under the terms of any of the licenses mentioned in (i), (ii) or (iii) (the \"Applicable License\"), you must comply with the terms of the Applicable License generally and the following provisions: (I) You must include a copy of, or the URI for, the Applicable License with every copy of each Adaptation You Distribute or Publicly Perform; (II) You may not offer or impose any terms on the Adaptation that restrict the terms of the Applicable License or the ability of the recipient of the Adaptation to exercise the rights granted to that recipient under the terms of the Applicable License; (III) You must keep intact all notices that refer to the Applicable License and to the disclaimer of warranties with every copy of the Work as included in the Adaptation You Distribute or Publicly Perform; (IV) when You Distribute or Publicly Perform the Adaptation, You may not impose any effective technological measures on the Adaptation that restrict the ability of a recipient of the Adaptation from You to exercise the rights granted to that recipient under the terms of the Applicable License. This Section 4(b) applies to the Adaptation as incorporated in a Collection, but this does not require the Collection apart from the Adaptation itself to be made subject to the terms of the Applicable License.\n" +
                    "If You Distribute, or Publicly Perform the Work or any Adaptations or Collections, You must, unless a request has been made pursuant to Section 4(a), keep intact all copyright notices for the Work and provide, reasonable to the medium or means You are utilizing: (i) the name of the Original Author (or pseudonym, if applicable) if supplied, and/or if the Original Author and/or Licensor designate another party or parties (e.g., a sponsor institute, publishing entity, journal) for attribution (\"Attribution Parties\") in Licensor's copyright notice, terms of service or by other reasonable means, the name of such party or parties; (ii) the title of the Work if supplied; (iii) to the extent reasonably practicable, the URI, if any, that Licensor specifies to be associated with the Work, unless such URI does not refer to the copyright notice or licensing information for the Work; and (iv) , consistent with Ssection 3(b), in the case of an Adaptation, a credit identifying the use of the Work in the Adaptation (e.g., \"French translation of the Work by Original Author,\" or \"Screenplay based on original Work by Original Author\"). The credit required by this Section 4(c) may be implemented in any reasonable manner; provided, however, that in the case of a Adaptation or Collection, at a minimum such credit will appear, if a credit for all contributing authors of the Adaptation or Collection appears, then as part of these credits and in a manner at least as prominent as the credits for the other contributing authors. For the avoidance of doubt, You may only use the credit required by this Section for the purpose of attribution in the manner set out above and, by exercising Your rights under this License, You may not implicitly or explicitly assert or imply any connection with, sponsorship or endorsement by the Original Author, Licensor and/or Attribution Parties, as appropriate, of You or Your use of the Work, without the separate, express prior written permission of the Original Author, Licensor and/or Attribution Parties.\n" +
                    "Except as otherwise agreed in writing by the Licensor or as may be otherwise permitted by applicable law, if You Reproduce, Distribute or Publicly Perform the Work either by itself or as part of any Adaptations or Collections, You must not distort, mutilate, modify or take other derogatory action in relation to the Work which would be prejudicial to the Original Author's honor or reputation. Licensor agrees that in those jurisdictions (e.g. Japan), in which any exercise of the right granted in Section 3(b) of this License (the right to make Adaptations) would be deemed to be a distortion, mutilation, modification or other derogatory action prejudicial to the Original Author's honor and reputation, the Licensor will waive or not assert, as appropriate, this Section, to the fullest extent permitted by the applicable national law, to enable You to reasonably exercise Your right under Section 3(b) of this License (right to make Adaptations) but not otherwise.\n" +
                    "5. Representations, Warranties and Disclaimer\n" +
                    "\n" +
                    "UNLESS OTHERWISE MUTUALLY AGREED TO BY THE PARTIES IN WRITING, LICENSOR OFFERS THE WORK AS-IS AND MAKES NO REPRESENTATIONS OR WARRANTIES OF ANY KIND CONCERNING THE WORK, EXPRESS, IMPLIED, STATUTORY OR OTHERWISE, INCLUDING, WITHOUT LIMITATION, WARRANTIES OF TITLE, MERCHANTIBILITY, FITNESS FOR A PARTICULAR PURPOSE, NONINFRINGEMENT, OR THE ABSENCE OF LATENT OR OTHER DEFECTS, ACCURACY, OR THE PRESENCE OF ABSENCE OF ERRORS, WHETHER OR NOT DISCOVERABLE. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF IMPLIED WARRANTIES, SO SUCH EXCLUSION MAY NOT APPLY TO YOU.\n" +
                    "\n" +
                    "6. Limitation on Liability. EXCEPT TO THE EXTENT REQUIRED BY APPLICABLE LAW, IN NO EVENT WILL LICENSOR BE LIABLE TO YOU ON ANY LEGAL THEORY FOR ANY SPECIAL, INCIDENTAL, CONSEQUENTIAL, PUNITIVE OR EXEMPLARY DAMAGES ARISING OUT OF THIS LICENSE OR THE USE OF THE WORK, EVEN IF LICENSOR HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.\n" +
                    "\n" +
                    "7. Termination\n" +
                    "\n" +
                    "This License and the rights granted hereunder will terminate automatically upon any breach by You of the terms of this License. Individuals or entities who have received Adaptations or Collections from You under this License, however, will not have their licenses terminated provided such individuals or entities remain in full compliance with those licenses. Sections 1, 2, 5, 6, 7, and 8 will survive any termination of this License.\n" +
                    "Subject to the above terms and conditions, the license granted here is perpetual (for the duration of the applicable copyright in the Work). Notwithstanding the above, Licensor reserves the right to release the Work under different license terms or to stop distributing the Work at any time; provided, however that any such election will not serve to withdraw this License (or any other license that has been, or is required to be, granted under the terms of this License), and this License will continue in full force and effect unless terminated as stated above.\n" +
                    "8. Miscellaneous\n" +
                    "\n" +
                    "Each time You Distribute or Publicly Perform the Work or a Collection, the Licensor offers to the recipient a license to the Work on the same terms and conditions as the license granted to You under this License.\n" +
                    "Each time You Distribute or Publicly Perform an Adaptation, Licensor offers to the recipient a license to the original Work on the same terms and conditions as the license granted to You under this License.\n" +
                    "If any provision of this License is invalid or unenforceable under applicable law, it shall not affect the validity or enforceability of the remainder of the terms of this License, and without further action by the parties to this agreement, such provision shall be reformed to the minimum extent necessary to make such provision valid and enforceable.\n" +
                    "No term or provision of this License shall be deemed waived and no breach consented to unless such waiver or consent shall be in writing and signed by the party to be charged with such waiver or consent.\n" +
                    "This License constitutes the entire agreement between the parties with respect to the Work licensed here. There are no understandings, agreements or representations with respect to the Work not specified here. Licensor shall not be bound by any additional provisions that may appear in any communication from You. This License may not be modified without the mutual written agreement of the Licensor and You.\n" +
                    "The rights granted under, and the subject matter referenced, in this License were drafted utilizing the terminology of the Berne Convention for the Protection of Literary and Artistic Works (as amended on September 28, 1979), the Rome Convention of 1961, the WIPO Copyright Treaty of 1996, the WIPO Performances and Phonograms Treaty of 1996 and the Universal Copyright Convention (as revised on July 24, 1971). These rights and subject matter take effect in the relevant jurisdiction in which the License terms are sought to be enforced according to the corresponding provisions of the implementation of those treaty provisions in the applicable national law. If the standard suite of rights granted under applicable copyright law includes additional rights not granted under this License, such additional rights are deemed to be included in the License; this License is not intended to restrict the license of any rights under applicable law.\n" +
                    "Creative Commons Notice\n" +
                    "Creative Commons is not a party to this License, and makes no warranty whatsoever in connection with the Work. Creative Commons will not be liable to You or any party on any legal theory for any damages whatsoever, including without limitation any general, special, incidental or consequential damages arising in connection to this license. Notwithstanding the foregoing two (2) sentences, if Creative Commons has expressly identified itself as the Licensor hereunder, it shall have all rights and obligations of Licensor.\n" +
                    "\n" +
                    "Except for the limited purpose of indicating to the public that the Work is licensed under the CCPL, Creative Commons does not authorize the use by either party of the trademark \"Creative Commons\" or any related trademark or logo of Creative Commons without the prior written consent of Creative Commons. Any permitted use will be in compliance with Creative Commons' then-current trademark usage guidelines, as may be published on its website or otherwise made available upon request from time to time. For the avoidance of doubt, this trademark restriction does not form part of the License.\n" +
                    "\n" +
                    "Creative Commons may be contacted at https://creativecommons.org/.";
    }
}
