/*
 InstaLate (instant translation app)
 Copyright (C) 2021 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.unn;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.zip.DeflaterInputStream;
import java.util.zip.GZIPInputStream;

import javax.net.ssl.HttpsURLConnection;

public class NetworkUtils {

    public static final String TAG = NetworkUtils.class.getSimpleName();
    public static final String ERR_CONNECTION_TIMEOUT = "ERR_CONNECTION_TIMEOUT";
    public static final String ERR_NO_CONNECTION = "ERR_NO_CONNECTION";
    public static final String ERR_NO_NETWORK = "ERR_NO_NETWORK";
    public static final String ERR_NO_DATA = "ERR_NO_DATA";

    public static PageResponse getPageSource(PageRequest request) {
        PageResponse response = new PageResponse();
        if (!request.getUri().toString().isEmpty()) {
            String urlStr = request.getUri().toString();
            HttpsURLConnection httpsConnection = null;
            BufferedReader reader = null;
            InputStream inputStream;

            try {
                URL requestUrl = new URL(urlStr);
                httpsConnection = (HttpsURLConnection) requestUrl.openConnection();
                if (request.getHTTPHeaders() != null) {
                    request.getHTTPHeaders().apply(httpsConnection);
                }
                httpsConnection.setConnectTimeout(20000);
                httpsConnection.setReadTimeout(20000);
                String reqMeth = request.getMethod();
                httpsConnection.setRequestMethod(reqMeth);
                switch (reqMeth) {
                    case PageRequest.PATCH:
                    case PageRequest.PUT:
                    case PageRequest.POST:
                        byte[] payload = request.getData().getBytes(StandardCharsets.UTF_8);
                        httpsConnection.setFixedLengthStreamingMode(payload.length);
                        httpsConnection.setDoOutput(true);
                        httpsConnection.connect();
                        try (OutputStream outputStream = httpsConnection.getOutputStream()) {
                            outputStream.write(payload);
                        }
                        break;
                    default:
                        httpsConnection.connect();
                        break;
                }

                if (httpsConnection.getResponseCode() != HttpsURLConnection.HTTP_OK) {
                    response.setError(ERR_NO_CONNECTION, urlStr);
                } else {

                    inputStream = httpsConnection.getInputStream();

                    if (httpsConnection.getContentEncoding() != null) {     // "[...] Android adds the "Accept-Encoding: gzip" header [and uncompresses!] automatically, but only if you didn't add it yourself first" (see: https://stackoverflow.com/a/42346308)
                        switch (httpsConnection.getContentEncoding()) {
                            case "gzip":
                                inputStream = new GZIPInputStream(inputStream);
                                break;
                            case "deflate":
                                inputStream = new DeflaterInputStream(inputStream);
                                break;
                            default:
                                break;
                        }
                    }

                    reader = new BufferedReader(new InputStreamReader(inputStream, request.getCharset()));

                    StringBuilder builder = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        builder.append(line);
                        builder.append("\n");
                    }
                    if (builder.length() == 0) {
                        response.setError(ERR_NO_DATA, urlStr);
                    } else {
                        response.set(builder.toString());
                    }
                }

            } catch (SocketTimeoutException e) {
                e.printStackTrace();
                response.setError(ERR_CONNECTION_TIMEOUT, urlStr);
            } catch (Exception e) {
                e.printStackTrace();
                response.setError(ERR_NO_CONNECTION, urlStr);
            } finally {
                if (httpsConnection != null)
                    httpsConnection.disconnect();
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return response;
    }

    public static boolean isConnected(Context ctx) {
        ConnectivityManager connectivityManager = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = null;
        if (connectivityManager != null) {
            networkInfo = connectivityManager.getActiveNetworkInfo();
        }
        return (networkInfo != null && networkInfo.isConnected());
    }

    public static void view(Context c, Uri uri) {
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        view(c, uri, intent);
    }
    public static void view(Context c, Uri uri, int intentFlags) {
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        intent.setFlags(intentFlags);
        view(c, uri, intent);
    }
    private static void view(Context c, Uri uri, Intent intent) {
        try {        //  since API 30 (A11,R) <queries> is required in manifest for resolveActivity() (see here: https://stackoverflow.com/a/62856745) since we only start an Activity we don't really need to qeuery "browseable" Activities but just catch ActivityNotFoundException instead
            c.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Log.w(TAG, "Can't handle implicit intent to open webpage " + uri.toString());
        }
    }


    public static class PageSourceTask extends AsyncTask<PageRequest, Void, PageResponse> {

        public interface OnResultListener {
            public void onResponse(PageResponse response);
        }

        private final WeakReference<OnResultListener> mListener;

        public PageSourceTask(OnResultListener listener) {
            mListener = new WeakReference<>(listener);
        }

        @Override
        protected PageResponse doInBackground(PageRequest... pageRequests) {
            return NetworkUtils.getPageSource(pageRequests[0]);
        }


        @Override
        protected void onPostExecute(PageResponse pageResponse) {
            super.onPostExecute(pageResponse);
            OnResultListener l = mListener.get();
            if (DroidUtils.isNonNullWeakRef(mListener)) {
                mListener.get().onResponse(pageResponse);
            }
        }
    }
}



