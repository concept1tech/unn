/*
 InstaLate (instant translation app)
 Copyright (C) 2021 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.unn;

import android.os.Parcel;
import android.os.Parcelable;

import java.net.URLConnection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

public class HTTPHeaders implements Parcelable {
    public static final int FIREFOX_85_UBUNTU_US = 1;
    public static final int FIREFOX_85_WIN_US = 2;
    public static final int FIREFOX_CUR_WIN_US = 3;

    public static HTTPHeaders getPreset(int preset) {
        HTTPHeaders headers;
        switch (preset) {
            case FIREFOX_85_UBUNTU_US:
                headers = new HTTPHeaders();
                headers.setHeader("User-Agent", "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:85.0) Gecko/20100101 Firefox/85.0");
                headers.setHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
                headers.setHeader("Accept-Language", "en-US,en;q=0.5");
                headers.setHeader("Accept-Encoding", "gzip, deflate, br");
                headers.setHeader("Connection", "keep-alive");
                headers.setHeader("Upgrade-Insecure-Requests", "1");
                headers.setHeader("Cache-Control", "max-age=0");
                break;
            case FIREFOX_85_WIN_US:
                headers = getPreset(FIREFOX_85_UBUNTU_US);
                headers.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:85.0) Gecko/20100101 Firefox/85.0");
                break;
            case FIREFOX_CUR_WIN_US:
                headers = getPreset(FIREFOX_85_UBUNTU_US);
                headers.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:95.0) Gecko/20100101 Firefox/95.0");
                break;
            default:
                headers = new HTTPHeaders();
                break;
        }
        return headers;
    }


    private Map<String, String> mHeaders = new LinkedHashMap<>();  // order of headers matters

    public HTTPHeaders() {

    }

    public HTTPHeaders(String... pairs) {
        for (int i = 0; i < pairs.length / 2; i += 2) {
            mHeaders.put(pairs[i], pairs[i + 1]);
        }
    }

    public void setHeader(String k, String v) {
        mHeaders.put(k, v);
    }
    public void getHeader(String k) {
        mHeaders.get(k);
    }
    public void apply(URLConnection urlConnection) {
        for (Map.Entry<String, String> entry : mHeaders.entrySet()) {
            urlConnection.setRequestProperty(entry.getKey(), entry.getValue());
        }
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HTTPHeaders that = (HTTPHeaders) o;
        return Objects.equals(mHeaders, that.mHeaders);
    }
    @Override
    public int hashCode() {
        return Objects.hash(mHeaders);
    }


    protected HTTPHeaders(Parcel in) {
        mHeaders = DroidUtils.readStringMapFromParcel(in, mHeaders);
    }
    public static final Creator<HTTPHeaders> CREATOR = new Creator<HTTPHeaders>() {
        @Override
        public HTTPHeaders createFromParcel(Parcel in) {
            return new HTTPHeaders(in);
        }

        @Override
        public HTTPHeaders[] newArray(int size) {
            return new HTTPHeaders[size];
        }
    };
    @Override
    public int describeContents() {
        return 0;
    }
    @Override
    public void writeToParcel(Parcel parcel, int i) {
        DroidUtils.writeStringMapToParcel(parcel, mHeaders);
    }
}
