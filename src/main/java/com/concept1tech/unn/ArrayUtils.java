/*
 InstaLate (instant translation app)
 Copyright (C) 2021 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.unn;

import android.util.Log;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

public class ArrayUtils {

    public static final String TAG = ArrayUtils.class.getSimpleName();

    public static final int FIRST = 0x1;
    public static final int LAST = 0x2;
    public static final int ASCEND = 0x4;
    public static final int DESCEND = 0x8;

    public static void permuteWith(String[][] mat, String[] arr1, String[] arr2, String format) {
        for (int i = 0; i < arr2.length; i++) {
            for (int j = 0; j < arr1.length; j++) {
                if (arr1[j] != null && arr2[i] != null)
                    mat[i][j] = String.format(Locale.US, format, arr2[i], arr1[j]);
            }
        }
    }

    public static void logMatrix(Object[][] mat) {
        logMatrix(mat, null, null);
    }

    public static void logMatrix(Object[][] mat, String[] rowDesc, String[] colDesc) {
        logMatrix(mat, rowDesc, colDesc, 12);
    }

    public static void logMatrix(Object[][] mat, String[] rowDesc, String[] colDesc, int maxCellWidth) {
        int minCellWidth = 1;
        String nullChar = " n* ";

        Integer[] rowNums = arange(0, mat.length, 1);
        Integer[] colNums = arange(0, mat[0].length, 1);

        String[] rowNumsF = format(" %d.", rowNums);
        String[] colNumsF = format(" %d.", colNums);

        String[][] matS = toString(mat, nullChar);

        // add numbers to rows
        for (int i = 0; i < matS.length; i++) {
            matS[i] = insert(matS[i], 0, rowNumsF[i]);
        }
        // add numbers to cols
        colNumsF = insert(colNumsF, 0, "");
        matS = insert(matS, 0, colNumsF);

        // add desc to rows
        if (rowDesc != null) {
            String[] rowDescF = toString(rowDesc, nullChar);  // convert nulls
            matS[0] = insert(matS[0], 0, "");
            for (int i = 0; i < rowDescF.length; i++) {
                matS[i + 1] = insert(matS[i + 1], 0, rowDescF[i]);
            }
        }
        // add desc to cols
        if (colDesc != null) {
            String[] colDescF = toString(colDesc, nullChar);  // convert nulls
            String[] twoCells = {"", ""};
            colDescF = insert(colDescF, 0, twoCells);
            matS = insert(matS, 0, colDescF);
        }

        int lenCell = minCellWidth;
        lenCell = Math.max(getMaxLen(matS), lenCell);
        lenCell = Math.min(maxCellWidth, lenCell);

        String format = String.format(Locale.US, "%%-%1$d.%1$ds", lenCell);
        Log.d(TAG, "\n--------\n");
        for (String[] row : matS) {
            StringBuilder builder = new StringBuilder();
            for (String elem : row) {
                builder.append(String.format(Locale.US, format, elem));
                builder.append("|");
            }
            Log.d(TAG, builder.toString());
        }
        Log.d(TAG, "\n--------\n");
    }

    public static void logMatrices(Object[][][] mats) {
        for (Object[][] mat : mats) {
            logMatrix(mat);
        }
    }

    public static void logMatrices(Object[][][] mats, String[] rowDesc, String[] colDesc) {
        for (Object[][] mat : mats) {
            logMatrix(mat, rowDesc, colDesc);
        }
    }

    public static void logMatrices(Object[][][] mats, String[] rowDesc, String[] colDesc, int maxCellWidth) {
        for (Object[][] mat : mats) {
            logMatrix(mat, rowDesc, colDesc, maxCellWidth);
        }
    }

    public static String[][] insert(String[][] mat1, int pos, String[][] mat2) {
        String[][] out = new String[mat1.length + mat2.length][];
        for (int i = 0; i < pos; i++) {
            out[i] = mat1[i].clone();
        }
        for (int i = 0; i < mat2.length; i++) {
            out[pos + i] = mat2[i].clone();
        }
        for (int i = pos; i < mat1.length; i++) {
            out[mat2.length + i] = mat1[i].clone();
        }
        return out;
    }

    public static String[][] insert(String[][] mat, int pos, String[] arr) {
        String[][] mat2 = new String[1][];
        mat2[0] = arr;
        return insert(mat, pos, mat2);
    }

    public static String[] insert(String[] arr1, int posArr1, String[] arr2, int posArr2, int lenArr2) {
        String[] out = new String[arr1.length + lenArr2];
        System.arraycopy(arr1, 0, out, 0, posArr1);
        System.arraycopy(arr2, posArr2, out, posArr1, lenArr2);
        System.arraycopy(arr1, posArr1, out, posArr1 + lenArr2, arr1.length - posArr1);
        return out;
    }

    public static String[] insert(String[] arr1, int posArr1, String[] arr2, int posArr2) {
        return insert(arr1, posArr1, arr2, posArr2, arr2.length);
    }

    public static String[] insert(String[] arr1, int posArr1, String[] arr2) {
        return insert(arr1, posArr1, arr2, 0, arr2.length);
    }

    public static String[] insert(String[] arr, int pos, String elem) {
        String[] arr2 = {elem};
        return insert(arr, pos, arr2, 0, 1);
    }

    public static String[] append(String[] arr, String elem) {
        return insert(arr, arr.length, elem);
    }

    public static String[] prepend(String[] arr, String elem) {
        return insert(arr, 0, elem);
    }

    public static String[] format(String format, Object[]... arrays) {
        int numArgs = arrays.length;
        int len = arrays[0].length;
        String[] out = new String[len];
        for (int i = 0; i < len; i++) {
            Object[] varArgs = new Object[numArgs];
            for (int j = 0; j < numArgs; j++) {
                varArgs[j] = arrays[j][i];
            }
            out[i] = String.format(Locale.US, format, varArgs);
        }
        return out;
    }

    public static Integer[] arange(Integer start, Integer end, Integer increment) {
        // TODO: Handle negative start, end, increments etc. Reuse for other numeric types
        Integer[] out = new Integer[(int) Math.ceil((end - start) / (double) increment)];
        int cnt = 0;
        for (int i = start; i < end; i += increment) {
            out[cnt++] = i;
        }
        return out;
    }

    public static int getMaxLen(String[] arr) {
        int maxLen = 0;
        for (String elem : arr) {
            maxLen = Math.max(elem.length(), maxLen);
        }
        return maxLen;
    }

    public static int getMaxLen(String[][] arr) {
        int maxLen = 0;
        for (String[] row : arr) {
            maxLen = Math.max(getMaxLen(row), maxLen);
        }
        return maxLen;
    }

    public static String[] toString(final Object[] in, final String nullChar) {
        String[] out = new String[in.length];
        for (int i = 0; i < in.length; i++) {
            Object elem = in[i];
            out[i] = elem == null ? nullChar : String.valueOf(in[i]);
        }
        return out;
    }

    public static String[] toString(Object[] in) {
        return toString(in, "null");
    }

    public static String[][] toString(final Object[][] in, final String nullChar) {
        String[][] out = new String[in.length][];
        for (int i = 0; i < in.length; i++) {
            out[i] = toString(in[i], nullChar);
        }
        return out;
    }

    public static String[][] toString(final Object[][] in) {
        return toString(in, "null");
    }

    public static Integer[] parseInt(String[] arrS) {
        Integer[] arrI = new Integer[arrS.length];
        for (int i = 0; i < arrS.length; i++)
            arrI[i] = Integer.parseInt(arrS[i]);
        return arrI;
    }

    public static Boolean[] toBool(Object[] src) {
        // element is false if: "", null, 0, 0f, -0f, 0d, -0d, \u0000
        Boolean[] dest = new Boolean[src.length];
        if (src instanceof CharSequence[]) {
            CharSequence[] srcC = (CharSequence[]) src;
            for (int i = 0; i < srcC.length; i++) {
                dest[i] = srcC[i] != null && srcC[i].length() != 0;
            }
        } else if (src instanceof Boolean[]) {
            Boolean[] srcC = (Boolean[]) src;
            for (int i = 0; i < srcC.length; i++) {
                dest[i] = srcC[i] != null && srcC[i];
            }
        } else if (src instanceof Byte[]) {
            Byte[] srcC = (Byte[]) src;
            for (int i = 0; i < srcC.length; i++) {
                dest[i] = srcC[i] != null && srcC[i] != 0;
            }
        } else if (src instanceof Short[]) {
            Short[] srcC = (Short[]) src;
            for (int i = 0; i < srcC.length; i++) {
                dest[i] = srcC[i] != null && srcC[i] != 0;
            }
        } else if (src instanceof Integer[]) {
            Integer[] srcC = (Integer[]) src;
            for (int i = 0; i < srcC.length; i++) {
                dest[i] = srcC[i] != null && srcC[i] != 0;
            }
        } else if (src instanceof Long[]) {
            Long[] srcC = (Long[]) src;
            for (int i = 0; i < srcC.length; i++) {
                dest[i] = srcC[i] != null && srcC[i] != 0;
            }
        } else if (src instanceof Character[]) {
            Character[] srcC = (Character[]) src;
            for (int i = 0; i < srcC.length; i++) {
                dest[i] = srcC[i] != null && srcC[i] != 0;
            }
        } else if (src instanceof Float[]) {
            Float[] srcC = (Float[]) src;
            for (int i = 0; i < srcC.length; i++) {
                dest[i] = srcC[i] != null && srcC[i] != 0f && srcC[i] != -0f;
            }
        } else if (src instanceof Double[]) {
            Double[] srcC = (Double[]) src;
            for (int i = 0; i < srcC.length; i++) {
                dest[i] = srcC[i] != null && srcC[i] != 0d && srcC[i] != -0d;
            }
        } else {
            for (int i = 0; i < src.length; i++) {
                dest[i] = src[i] != null;
            }
        }
        return dest;
    }

    @SuppressWarnings("unchecked")
    public static <T> T[] get1dArrayFrom3d(Class<T> c, T[][][] mat, int varIdxDimension, int constIdxA, int constIdxB) {
        // Dimension 0 is outmost array: mat[dimension0][dimension1][dimension2]
        // --> lenDimension0 = mat.length;
        //
        // mat[i][c][c]: dimension 0 is varying (1 and 2 const)
        // mat[c][i][c]: dimension 1 is varying (0 and 2 const)
        // mat[c][c][i]: dimension 2 is varying (0 and 1 const)
        T[] arr;
        int len;
        switch (varIdxDimension) {
            default:
            case 0:
                len = mat.length;
                arr = (T[]) Array.newInstance(c, len);
                for (int i = 0; i < len; i++) {
                    arr[i] = mat[i][constIdxA][constIdxB];
                }
                return arr;
            case 1:
                len = mat[0].length;
                arr = (T[]) Array.newInstance(c, len);
                for (int i = 0; i < len; i++) {
                    arr[i] = mat[constIdxA][i][constIdxB];
                }
                return arr;
            case 2:
                arr = mat[constIdxA][constIdxB].clone();
                return arr;
        }
    }

    @SuppressWarnings("unchecked")
    public static <T> T[][] get2dArrayFrom3d(Class<T[]> c, T[][][] mat, int varIdxDimensionA, int varIdxDimensionB, int constIdx) {
        // Dimension 0 is outmost array: mat[dimension0][dimension1][dimension2]
        // --> lenDimension0 = mat.length;
        //
        // mat[i][i][c]: dimension 0 and 1 are varying (2 const), return new arr[dimension0][dimension1]
        // mat[i][c][i]: dimension 0 and 2 are varying (1 const), return new arr[dimension0][dimension2]
        // mat[c][i][i]: dimension 1 and 2 are varying (0 const), return new arr[dimension1][dimension2]
        T[][] arr;
        if (varIdxDimensionA + varIdxDimensionB == 1) {
            // varIdxDimensionA = 0 and varIdxDimensionB = 1 or vice versa
            int lenDimension0 = mat.length;
            int lenDimension1 = mat[0].length;
            arr = (T[][]) Array.newInstance(c, lenDimension0);
            for (int i = 0; i < lenDimension0; i++) {
                arr[i] = mat[0][0].clone();
                for (int k = 0; k < lenDimension1; k++) {
                    arr[i][k] = mat[i][k][constIdx];
                }
            }
            return arr;
        } else if (varIdxDimensionA + varIdxDimensionB == 2) {
            // varIdxDimensionA = 0 and varIdxDimensionB = 2 or vice versa
            int lenDimension0 = mat.length;
            arr = (T[][]) Array.newInstance(c, lenDimension0);
            for (int i = 0; i < lenDimension0; i++) {
                arr[i] = mat[i][constIdx].clone();
            }
            return arr;
        } else {
            // varIdxDimensionA = 1 and varIdxDimensionB = 2 or vice versa
            arr = cloneMat(mat[constIdx]);
            return arr;
        }
    }

    public static <T> LinkedList<T> removeIfValueInReference(List<T> dest, Object[] ref, Object val) {
        LinkedList<T> ll = new LinkedList<>(dest);
        for (int i = ref.length - 1; i >= 0; i--) {
            if (ref[i] == val) {
                ll.remove(i);
            }
        }
        return ll;
    }

    public static String[] removeIfValueInReference(String[] dest, Object[] ref, Object val) {
        Object[] oArr = removeIfValueInReferenceBase(dest, ref, val);
        return Arrays.copyOf(oArr, oArr.length, String[].class);
    }
    public static Integer[] removeIfValueInReference(Integer[] dest, Object[] ref, Object val) {
        Object[] oArr = removeIfValueInReferenceBase(dest, ref, val);
        return Arrays.copyOf(oArr, oArr.length, Integer[].class);
    }

    public static Object[] removeIfValueInReferenceBase(Object[] dest, Object[] ref, Object val) {
        int len = ref.length;
        Object[] tmp = new Object[len];
        int count = 0;
        for (int i = 0; i < len; i++) {
            if (ref[i] != val) {
                tmp[count] = dest[i];
                count++;
            }
        }
//        if (count<1) {} // TODO if something goes wrong
        return Arrays.copyOf(tmp, count);
    }

    public static String[] removeNulls(String[] arr) {
        return removeIfValueInReference(arr, arr, null);
    }

    public static void setFromArray(Object[][] mat, Object[] arr, int x, int y, int dimension) {
        setFromArray(mat, arr, x, y, dimension, false);
    }

    private static void setFromArray(Object[][] mat, Object[] arr, int x, int y, int dimension, boolean extendIfLarge) {
        // TODO: implement extendIfLarge (resize nested array)
        int lenArr = arr.length;
        if (dimension == 0) {
            int lenMat = mat[y].length;
            int lenMin = Math.min(lenMat - x, lenArr);
            if (lenMin >= 0) System.arraycopy(arr, 0, mat[y], x, lenMin);
        } else {
            int lenMat = mat.length;
            int lenMin = Math.min(lenMat - y, lenArr);
            for (int i = 0; i < lenMin; i++) {
                mat[i + y][x] = arr[i];
            }
        }
    }

    public static void setArea(Object[][] mat, int x1, int y1, int x2, int y2, Object val) {
        for (int y = y1; y <= y2; y++) {
            Arrays.fill(mat[y], x1, x2 + 1, val);
        }
    }

    public static void fill(Object[][] mat, Object val) {
        for (Object[] subArr : mat) {
            Arrays.fill(subArr, val);
        }
    }

    public static void setMainDiagonal(Object[][] mat, Object val) {
        // mat is a pointer to the array
        for (int i = 0; i < mat.length; i++) {
            mat[i][i] = val;
        }
    }

    public static final byte TOPRIGHT_TO_BOTTOMLEFT = 1; // top-right to bottom left
    public static final byte BOTTOMLEFT_TO_TOPRIGHT = 2; // bottom-left to top right
    public static void setSymmetric(Object[][] m, byte direction) {
        // Mirrors at main-diagonal. Sets upper-right triangular part of rectangular matrix
        // (or lower-left) to lower-left part (or upper-right)
        int numCols = m[0].length;
        int numRows = m.length;
        if (numCols != numRows) throw new ArithmeticException("Matrix is not rectangular");
        for (int i = 0; i < numRows - 1; i++) {
            for (int j = i + 1; j < numCols; j++) {
                if (direction == TOPRIGHT_TO_BOTTOMLEFT)
                    m[j][i] = m[i][j];
                else
                    m[i][j] = m[j][i];
            }
        }
    }

    public static void disjunctSymmetric(Boolean[][] m) {
        // Mirrors at main-diagonal. Disjuncts (logical OR) upper-right triangular part of rectangular matrix
        // with lower-left part
        Boolean[][] m2 = ArrayUtils.cloneMat(m);
        setSymmetric(m, TOPRIGHT_TO_BOTTOMLEFT);
        setSymmetric(m2, BOTTOMLEFT_TO_TOPRIGHT);
        oror(m, m2);
    }

    public static void conjunctSymmetric(Boolean[][] m) {
        // Mirrors at main-diagonal. Conjuncts (logical AND) upper-right triangular part of rectangular matrix
        // with lower-left part
        Boolean[][] m2 = ArrayUtils.cloneMat(m);
        setSymmetric(m, TOPRIGHT_TO_BOTTOMLEFT);
        setSymmetric(m2, BOTTOMLEFT_TO_TOPRIGHT);
        andand(m, m2);
    }

    public static <T> T[][] cloneMat(final T[][] src) {
        T[][] dest = src.clone();
        for (int i = 0; i < src.length; i++) {
            dest[i] = src[i].clone();
        }
        return dest;
    }

    public static void andand(Boolean[][] m1, Boolean[][] m2) {
        // returns elementwise logical AND of the intersection of Boolean matrix 1 and matrix 2, modifies matrix 1.
        // uses && instead of & because of performance
        int numCols = Math.min(m1[0].length, m2[0].length);
        int numRows = Math.min(m1.length, m2.length);
        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numCols; j++) {
                m1[i][j] = m1[i][j] && m2[i][j];
            }
        }
    }

    public static void oror(Boolean[][] m1, Boolean[][] m2) {
        // returns elementwise logical OR of the intersection of Boolean matrix 1 and matrix 2, modifies matrix 1.
        // uses || instead of | because of performance
        int numCols = Math.min(m1[0].length, m2[0].length);
        int numRows = Math.min(m1.length, m2.length);
        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numCols; j++) {
                m1[i][j] = m1[i][j] || m2[i][j];
            }
        }
    }

    public static <T> boolean contains(final T[] array, final T v) {
        if (v == null) {
            for (final T e : array)
                if (e == null)
                    return true;
        } else {
            for (final T e : array)
                if (e == v || v.equals(e))
                    return true;
        }

        return false;
    }


    public static Integer find(final Boolean[] in) {
        return find(in, FIRST);
    }
    public static Integer find(final Boolean[] in, final int direction) {
        if (direction == FIRST) {
            for (int i = 0; i < in.length; i++) {
                if (in[i]) {
                    return i;
                }
            }
        } else if (direction == LAST) {
            for (int i = in.length - 1; i >= 0; i--) {
                if (in[i]) {
                    return i;
                }
            }
        }
        return null;
    }

    public static Integer[] find(final Boolean[][] in) {
        return find(in, FIRST);
    }
    public static Integer[] find(final Boolean[][] in, final int direction) {
        if (direction == FIRST) {
            for (int i = 0; i < in.length; i++) {
                for (int j = 0; j < in[i].length; j++) {
                    if (in[i][j]) {
                        return new Integer[]{i, j};
                    }
                }
            }
        } else if (direction == LAST) {
            for (int i = in.length - 1; i >= 0; i--) {
                for (int j = in[i].length - 1; j >= 0; j--) {
                    if (in[i][j]) {
                        return new Integer[]{i, j};
                    }
                }
            }

        }
        return null;
    }

    public static Integer[] find(final Boolean[][][] in) {
        return find(in, FIRST);
    }
    public static Integer[] find(final Boolean[][][] in, final int direction) {
        if (direction == FIRST) {
            for (int i = 0; i < in.length; i++) {
                for (int j = 0; j < in[i].length; j++) {
                    for (int k = 0; k < in[i][j].length; k++) {
                        if (in[i][j][k]) {
                            return new Integer[]{i, j, k};
                        }
                    }
                }
            }
        } else if (direction == LAST) {
            for (int i = in.length - 1; i >= 0; i--) {
                for (int j = in[i].length - 1; j >= 0; j--) {
                    for (int k = in[i][j].length - 1; k >= 0; k--) {
                        if (in[i][j][k]) {
                            return new Integer[]{i, j, k};
                        }
                    }
                }
            }

        }
        return null;
    }

    public static <T extends Comparable<T>> Integer[] getIdxsOfSorted(final T[] reference, int order) {
        Integer[] idx = ArrayUtils.arange(0, reference.length, 1);
        if (order == ArrayUtils.DESCEND) {
            Arrays.sort(idx, new Comparator<Integer>() {
                @Override
                public int compare(Integer left, Integer right) {
                    return reference[right].compareTo(reference[left]);
                }
            });
        } else {
            Arrays.sort(idx, new Comparator<Integer>() {
                @Override
                public int compare(Integer left, Integer right) {
                    return reference[left].compareTo(reference[right]);
                }
            });
        }
        return idx;
    }

    public static <T extends Comparable<T>> Integer[] getIdxsOfSorted(final T[] reference) {
        return getIdxsOfSorted(reference, ArrayUtils.ASCEND);
    }

    public static <T> T[] getByIdxs(T[] destination, Integer[] idxs) {
        T[] out = destination.clone();
        for (int i = 0; i < idxs.length; i++) {
            out[i] = destination[idxs[i]];
        }
        return out;
    }

    public static Boolean[][] arrayToSymMatrix(Boolean[] arr) {
        Boolean[][] mat = new Boolean[arr.length][arr.length];
        ArrayUtils.fill(mat, false);
        for (int i = 0; i < mat.length; i++) {
            if (arr[i]) {
                ArrayUtils.setFromArray(mat, arr, 0, i, 0);
            }
        }
        return mat;
    }
}
