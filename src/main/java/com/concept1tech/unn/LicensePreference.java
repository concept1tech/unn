/*
 InstaLate (instant translation app)
 Copyright (C) 2021 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.unn;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import androidx.appcompat.app.AlertDialog;
import androidx.preference.Preference;

public class LicensePreference extends Preference {
    public LicensePreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initAttributes(context, attrs);
    }
    public LicensePreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttributes(context, attrs);
    }
    public LicensePreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAttributes(context, attrs);
    }
    public LicensePreference(Context context) {
        super(context);
    }

    private void initAttributes(Context c, AttributeSet as) {
        TypedArray ta = c.obtainStyledAttributes(as, R.styleable.LicensePreference);
        int license = ta.getInteger(R.styleable.LicensePreference_license, -1);
        String year = ta.getString(R.styleable.LicensePreference_year);
        String author = ta.getString(R.styleable.LicensePreference_author);
        String link = ta.getString(R.styleable.LicensePreference_link);
        ta.recycle();

        if (license < 0) {
            throw new IllegalStateException("Mandatory attribute 'license' missing");
        }
        if (year == null) {
            year = "<year>";
        }
        if (author == null) {
            author = "<author>";
        }
        if (link == null) {
            link = "";
        }


        setSummary(link);
        setIconSpaceReserved(false);
        final AlertDialog.Builder ab = new AlertDialog.Builder(c)
                    .setTitle(getTitle())
                    .setPositiveButton(R.string.Close, (DialogInterface dialog, int i) -> dialog.dismiss());
        switch (license) {
            case 0:
                ab.setMessage(LicenseUtils.mit(year, author));
                break;
            case 1:
                ab.setMessage(LicenseUtils.apache2());
                break;
            case 2:
                ab.setMessage(LicenseUtils.gplv3());
                break;
            case 3:
                ab.setMessage(LicenseUtils.cc_by_sa_3());
                break;
            case 4:
                ab.setMessage(LicenseUtils.gplv2());
                break;
            default:
                break;
        }
        setOnPreferenceClickListener((Preference preference) -> {
            ab.create().show();
            return true;
        });
    }
}
