/*
 InstaLate (instant translation app)
 Copyright (C) 2021 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.unn;

import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.CountDownTimer;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.widget.Toast;

import androidx.preference.PreferenceManager;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Map;

public class DroidUtils {

    public static void longToast(Toast toast, int numIntervals) {
        // Toast.LENGTH_LONG defaults to 3500ms. 'interval' is not guaranteed to be exactly that value
        // and if its earlier than 3500ms subsequent toasts will not show. So make we make 'interval' a bit longer.
        int interval = 3650;
        int totalDuration = interval / 2 + numIntervals * interval;
        new CountDownTimer(totalDuration, interval) {
            @Override
            public void onTick(long l) {
                toast.setDuration(Toast.LENGTH_LONG);
                toast.show();
            }

            @Override
            public void onFinish() {
            }
        }.start();
    }

    // Not ideal. Will return true also if the service is only bound but NOT yet started (onStartCommand() not called yet)!
    // Oreo: deprecated
    // KitKat: presumably not working?
    public static boolean isServiceRunning(Context ctx, Class<? extends Service> serviceClass) {
        ActivityManager manager = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);
        if (manager != null) {
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.getName().equals(service.service.getClassName())) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean hasOverlayPermission(Context ctx) {
        // "... If the app targets API level 23 or higher, the app user must explicitly grant this permission to the app through a permission management screen. The app requests the user's approval by sending an intent with action Settings.ACTION_MANAGE_OVERLAY_PERMISSION. The app can check whether it has this authorization by calling Settings.canDrawOverlays() ..."
        return ctx != null && Settings.canDrawOverlays(ctx.getApplicationContext());
    }


    // put shared pref methods
    public static void prefsPutBoolean(Context ctx, String key, boolean val) {
        SharedPreferences.Editor prefEditor = getPrefEditor(ctx);
        prefEditor.putBoolean(key, val);
        prefEditor.apply();     // or prefEditor.commit(); to apply immediately
    }
    public static void prefsPutInt(Context ctx, String key, int val) {
        SharedPreferences.Editor prefEditor = getPrefEditor(ctx);
        prefEditor.putInt(key, val);
        prefEditor.apply();     // or prefEditor.commit(); to apply immediately
    }
    public static void prefsPutString(Context ctx, String key, String val) {
        SharedPreferences.Editor prefEditor = getPrefEditor(ctx);
        prefEditor.putString(key, val);
        prefEditor.apply();     // or prefEditor.commit(); to apply immediately
    }
    private static SharedPreferences.Editor getPrefEditor(Context ctx) {
        SharedPreferences defaultSPref = PreferenceManager.getDefaultSharedPreferences(ctx);
        return defaultSPref.edit();
    }


    public static void writeStringMapToParcel(Parcel dest, Map<String, String> map) {
        if (map == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(map.size());
            for (Map.Entry<String, String> entry : map.entrySet()) {
                dest.writeString(entry.getKey());
                dest.writeString(entry.getValue());
            }
        }
    }
    public static Map<String, String> readStringMapFromParcel(Parcel in, Map<String, String> map) {
        if (in.readByte() == 0x01) {
            int size = in.readInt();
            for (int i = 0; i < size; i++) {
                String key = in.readString();
                String value = in.readString();
                map.put(key, value);
            }
        } else {
            map = null;
        }
        return map;
    }

    public static <T extends Parcelable> void writeArrayListToParcel(Parcel dest, List<T> list) {
        if (list == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(list);
        }
    }
    public static <T extends Parcelable> List<T> readListFromParcel(Parcel in, List<T> list, Class<T> cl) {
        if (in.readByte() == 0x01) {
            in.readList(list, cl.getClassLoader());
        } else {
            list = null;
        }
        return list;
    }

    /**
     * Writes Integer arrays. For primitive int arrays use writeIntArray().
     *
     * @param array Can be null and elements can be null too.
     */
    public static void writeIntegerArray(Parcel dest, Integer[] array) {
        if (array == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            int len = array.length;
            dest.writeInt(len);
            for (Integer integer : array) {
                if (integer == null) {
                    dest.writeByte((byte) (0x00));
                } else {
                    dest.writeByte((byte) (0x01));
                    dest.writeInt(integer);
                }
            }
        }
    }
    /**
     * (Re)creates Integer arrays. For primitive int arrays use createIntArray(). Recreates null elements too.
     */
    public static Integer[] createIntegerArray(Parcel in) {
        Integer[] array = null;
        if (in.readByte() == 0x01) {
            int len = in.readInt();
            array = new Integer[len];
            for (int i = 0; i < len; i++) {
                array[i] = in.readByte() == 0x01 ? in.readInt() : null;
            }
        }
        return array;
    }


    // "insets" see here: https://medium.com/androiddevelopers/gesture-navigation-handling-visual-overlaps-4aed565c134c
    public static float getScreenWidthMinusInsets(Context c) {
        Resources r = c.getResources();
        return dp2Px(r.getDisplayMetrics(), r.getConfiguration().screenWidthDp);
    }
    public static float getScreenHeightMinusInsets(Context c) {
        Resources r = c.getResources();
        return dp2Px(r.getDisplayMetrics(), r.getConfiguration().screenHeightDp);
    }


    public static float dp2Px(Context c, float dp) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, c.getResources().getDisplayMetrics());
    }
    public static float px2Dp(Context c, float px) {
        return px / dp2Px(c, 1);
    }
    public static float dp2Px(DisplayMetrics dpm, float dp) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, dpm);
    }
    public static float px2Dp(DisplayMetrics dpm, float px) {
        return px / dp2Px(dpm, 1);
    }
    public static float sp2Px(Context c, float sp) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, c.getResources().getDisplayMetrics());
    }
    public static float px2Sp(Context c, float px) {
        return px / sp2Px(c, 1);
    }
    public static float sp2Px(DisplayMetrics dpm, float sp) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, dpm);
    }
    public static float px2Sp(DisplayMetrics dpm, float px) {
        return px / sp2Px(dpm, 1);
    }


    public static int[] getResourcesArray(Context c, int id) {
        TypedArray tArr = c.getResources().obtainTypedArray(id);
        int[] ids = new int[tArr.length()];
        for (int i = 0; i < tArr.length(); i++) {
            ids[i] = tArr.getResourceId(i, 0);
        }
        tArr.recycle();
        return ids;
    }


    public static boolean isNonNullWeakRef(WeakReference<?> weakRef) {
        // check if GC already cleaned up our weak ref (shouldnt happen here though because app ctx)
        return weakRef != null && weakRef.get() != null;
    }
}
