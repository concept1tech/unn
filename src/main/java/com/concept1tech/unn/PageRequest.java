/*
 InstaLate (instant translation app)
 Copyright (C) 2021 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.unn;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.Map;
import java.util.Objects;

public class PageRequest implements Parcelable {

    public static final String CONNECT = "CONNECT";
    public static final String DELETE = "DELETE";
    public static final String GET = "GET";
    public static final String HEAD = "HEAD";
    public static final String OPTIONS = "OPTIONS";
    public static final String PATCH = "PATCH";
    public static final String POST = "POST";
    public static final String PUT = "PUT";
    public static final String TRACE = "TRACE";

    private Uri mUri = Uri.parse("");
    private String mCharset = "UTF-8";
    private HTTPHeaders mHTTPHeaders = new HTTPHeaders();
    private String mMethod = PageRequest.GET;
    private String mData = "";

    public PageRequest() {
    }
    public Uri getUri() {
        return mUri;
    }
    public void setUri(Uri uri) {
        mUri = uri;
    }

    public String getCharset() {
        return mCharset;
    }
    public void setCharset(String charset) {
        mCharset = charset;
    }

    public HTTPHeaders getHTTPHeaders() {
        return mHTTPHeaders;
    }
    public void setHTTPHeaders(HTTPHeaders headers) {
        mHTTPHeaders = headers;
    }

    public String getMethod() {
        return mMethod;
    }
    public void setMethod(String method) {
        mMethod = method;
    }

    public String getData() {
        return mData;
    }
    public void setData(String data) {
        mData = data;
    }
    public void setDataUrlEncoded(Map<String, String> data) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> entry : data.entrySet())
            sb.append(Uri.encode(entry.getKey()))
                        .append("=")
                        .append(Uri.encode(entry.getValue()))
                        .append("&");
        sb.setLength(sb.length() > 0 ? sb.length() - 1 : 0);
        mData = sb.toString();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PageRequest that = (PageRequest) o;
        return Objects.equals(mUri, that.mUri) &&
                    Objects.equals(mCharset, that.mCharset) &&
                    Objects.equals(mHTTPHeaders, that.mHTTPHeaders) &&
                    Objects.equals(mMethod, that.mMethod) &&
                    Objects.equals(mData, that.mData);
    }
    @Override
    public int hashCode() {
        return Objects.hash(mUri, mCharset, mHTTPHeaders, mMethod, mData);
    }


    protected PageRequest(Parcel in) {
        mUri = in.readParcelable(Uri.class.getClassLoader());
        mCharset = in.readString();
        mHTTPHeaders = in.readParcelable(HTTPHeaders.class.getClassLoader());
        mMethod = in.readString();
        mData = in.readString();
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(mUri, flags);
        dest.writeString(mCharset);
        dest.writeParcelable(mHTTPHeaders, flags);
        dest.writeString(mMethod);
        dest.writeString(mData);
    }
    @Override
    public int describeContents() {
        return 0;
    }
    public static final Creator<PageRequest> CREATOR = new Creator<PageRequest>() {
        @Override
        public PageRequest createFromParcel(Parcel in) {
            return new PageRequest(in);
        }

        @Override
        public PageRequest[] newArray(int size) {
            return new PageRequest[size];
        }
    };
}
