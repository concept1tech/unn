/*
 InstaLate (instant translation app)
 Copyright (C) 2021 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.unn;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

/**
 * Wrapper class for a parcelable CharSequence and all its subclasses
 */
public class ParcelableCharSequence implements Parcelable {

    private final CharSequence mCharSequence;

    public ParcelableCharSequence(CharSequence charSequence) {
        mCharSequence = charSequence;
    }

    public CharSequence get() {
        return mCharSequence;
    }


    protected ParcelableCharSequence(Parcel in) {
        mCharSequence = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(in);
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        TextUtils.writeToParcel(mCharSequence, dest, flags);
    }
    @Override
    public int describeContents() {
        return 0;
    }
    public static final Creator<ParcelableCharSequence> CREATOR = new Creator<ParcelableCharSequence>() {
        @Override
        public ParcelableCharSequence createFromParcel(Parcel in) {
            return new ParcelableCharSequence(in);
        }

        @Override
        public ParcelableCharSequence[] newArray(int size) {
            return new ParcelableCharSequence[size];
        }
    };
}
